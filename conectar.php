<?php
error_reporting(E_ALL ^ E_NOTICE);
session_start("vigo_central");

if (!ini_get("register_globals")) { @import_request_variables('gp'); }

//setlocale(LC_TIME,"ptb","pt_BR","portuguese-brazil","bra","brazil","pt_BR.utf-8","pt_BR.iso-8859-1","br");

set_time_limit(40);

ini_set("log_errors","On");
ini_set("error_log","error_log");

$ModeloBoleto = "boleto"; # boleto, boletoanatel
$MostrarLogoVigo = 'S';
$MostrarArrobaDominio = 'S';

global $vigo;
$vigo=Conexao::conn("vigo");

//global $mikrotik;
//$mikrotik=Conexao::conn("mikrotik");

global $v_title;
$v_title="Central ".ucfirst($_SESSION["cid"]);

global $box;
$box=$_REQUEST["box"];

global $a_semana;
$a_semana=array("Sunday"=>"Domingo","Monday"=>"Segunda-feira","Tuesday"=>"Ter�a-feira","Wednesday"=>"Quarta-feira","Thursday"=>"Quinta-feira","Friday"=>"Sexta-feira","Saturday"=>"S�bado");

global $a_meses;
$a_meses=array("","Janeiro","Fevereiro","Mar�o","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro");

global $a_estados;
$a_estados = array(
	"AC"=>"Acre",
	"AL"=>"Alagoas",
	"AP"=>"Amap�",
	"AM"=>"Amazonas",
	"BA"=>"Bahia",
	"CE"=>"Cear�",
	"DF"=>"Distrito Federal",
	"ES"=>"Esp�rito Santo",
	"GO"=>"Goi�s",
	"MA"=>"Maranh�o",
	"MG"=>"Minas Gerais",
	"MT"=>"Mato Grosso",
	"MS"=>"Mato Grosso do Sul",
	"PA"=>"Par�",
	"PR"=>"Paran�",
	"PE"=>"Pernambuco",
	"PI"=>"Piau�",
	"RJ"=>"Rio de Janeiro",
	"RN"=>"Rio Grande do Norte",
	"RS"=>"Rio Grande do Sul",
	"RO"=>"Rond�nia",
	"RR"=>"Roraima",
	"SC"=>"Santa Catarina",
	"SP"=>"S�o Paulo",
	"SE"=>"Sergipe",
	"TO"=>"Tocantins"
);

global $v_diretorio;
$v_diretorio=(file_exists("../../../../conectar.php"))?4:$v_diretorio;
$v_diretorio=(file_exists("../../../conectar.php"))?3:$v_diretorio;
$v_diretorio=(file_exists("../../conectar.php"))?2:$v_diretorio;
$v_diretorio=(file_exists("../conectar.php"))?1:$v_diretorio;
$v_diretorio=(file_exists("conectar.php"))?0:$v_diretorio;
$v_diretorio=@str_repeat("../",$v_diretorio);

function __autoload($class_name)
{
    include "classes/{$class_name}.php";
}
?>