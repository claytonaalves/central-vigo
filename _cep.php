<?php
ob_clean();

header("Content-Type: text/html; charset=iso-8859-1");

$retorno = Suporte::curl_get_file_contents('http://servidor.datweb.com.br/cep/?cep='.urlencode($q).'&formato=string');

parse_str($retorno,$resultado);

if($q=="")
{
	?>
	Digite um cep para pesquisar.
	<?php
	exit();
}

if(count($resultado)>0)
{
	
	$t_form="frm_cadastro";
	$form=($form=="")?$t_form:$form;
	
	if($dest=="usuarios")
	{
		?>
		document.<?=$form?>.siu_uf.value='<?=$resultado["uf"]?>';
		document.<?=$form?>.siu_cidade.value='<?=$resultado["cidade"]?>';
		document.<?=$form?>.siu_bairro.value='<?=$resultado["bairro"]?>';		
		<?php
	}
	
	if($dest=="franquias")
	{
		?>
		document.<?=$form?>.sif_uf.value='<?=$resultado["uf"]?>';
		document.<?=$form?>.sif_cidade.value='<?=$resultado["cidade"]?>';
		document.<?=$form?>.sif_bairro.value='<?=$resultado["bairro"]?>';
		<?php
	}
	
	if($dest=="sysrad_ouvintes")
	{
		?>
		document.<?=$form?>.OUV_ESTADO.value='<?=$resultado["uf"]?>';
		document.<?=$form?>.OUV_CIDADE.value='<?=$resultado["cidade"]?>';
		document.<?=$form?>.OUV_BAIRRO.value='<?=$resultado["bairro"]?>';
		<?php
	}
	
}
else
{
	?>
	alert("Nada encontrado");
	<?php
}

if($debug!="")
{
	var_dump($_REQUEST,$retorno,$resultado);
}

exit();
?>