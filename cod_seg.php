<?php
session_start();

$imagem = imagecreate(50, 25);

$fundo = imagecolorallocate($imagem, 10, 50, 105);

$fonte = imagecolorallocate($imagem, 255, 255, 255);

imagestring($imagem, 4, 6, 3, $_SESSION["autenticagd"], $fonte);

header("Content-type: image/png");

imagepng($imagem);
?>
