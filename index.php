<?php
$tempo=microtime(true);

include("conectar.php");
include("framework/class_imagizer.php");

$qstring=explode("&",$_SERVER['QUERY_STRING']);
$pagina=$qstring[0];
$pagina=Suporte::nomear($pagina);

if(!in_array($_SESSION["cid"],array("usuario","operador")))
{
    exit(header("Location: usuario/"));
}

$pasta="pags_{$_SESSION["cid"]}";
$destino="{$pasta}/{$pagina}.php";

if(!file_exists($destino))
{
	$pagina="index";
}

if($_SESSION["cid"]=="usuario")
{
	if(!Usuarios::logado())
	{
		$pagina="login";
	}
//	else
//	{
//		if(!Usuarios::contratoAssinado() AND Central::permissao("contrato_obrigatorio") AND $pagina!="sair")
//		{
//			$pagina="contrato";
//		}
//	}
	Central::permissaoUsuario($pagina);
}

if($_SESSION["cid"]=="operador")
{
	if(!Operadores::logado())
	{
		$pagina="login";
	}
}

function title($buffer)
{
	global $v_title2;
	if($v_title2!="")
	{
		$buffer=str_replace("<title>","<title>{$v_title2} - ",$buffer);
	}
	return $buffer;
}
ob_start("title");

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=windows-1252"/>
	<title><?php echo $v_title?></title>
	
	<?php
	if($v_base!="")
	{
		?>
		<base href="<?=$v_base?>"/>
		<?php
	}
	?>
	
	<link rel="stylesheet" type="text/css" href="framework/framework.css"/>
	<link rel="stylesheet" type="text/css" href="css.css?<?=filemtime("css.css")?>"/>
	<link rel="stylesheet" type="text/css" href="cores/azul.css"/>
	<?php
	if($box!="")
	{
		?>
		<link rel="stylesheet" type="text/css" href="css_box.css"/>
		<?php
	}
	?>
	
	<link rel="shortcut icon" href="favicon.ico"/>
	
	<script type="text/javascript" src="framework/jquery-1.5.1.min.js"></script>
	
	<link type="text/css" href="framework/smoothness/jquery-ui-1.8.13.custom.css" rel="stylesheet" />	
	<script type="text/javascript" src="framework/jquery-ui-1.8.13.custom.min.js"></script>
	
	<script type="text/javascript" src="framework/jquery.ui.datepicker-pt-BR.js"></script>
	<script type="text/javascript" src="framework/jquery.ui.timepicker.js"></script>
	<script type="text/javascript" src="framework/jquery.maskedinput-1.2.2.js"></script>
	<script type="text/javascript" src="framework/jquery.maskMoney.0.2.js"></script>
	<script type="text/javascript" src="framework/jquery.alphanumeric.js"></script>
	
	<script type="text/javascript" language="javascript" src="framework/fancybox/jquery.fancybox-1.2.6.js"></script>
	<link rel="stylesheet" href="framework/fancybox/jquery.fancybox-1.2.6.css" type="text/css" media="screen"/>
	
	<script type="text/javascript" src="framework/fusioncharts/fusioncharts.js"></script>
	
	<script type="text/javascript" src="framework/ajaxutil.js"></script>
	<script type="text/javascript" src="javascript.js"></script>
	
	<script type="text/javascript" src="framework/framework_mask.js"></script>

	<!--[if lt IE 9]>
	<script src"http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

</head>

<body>

<div id="rd-geral">

	<div id="rd-topo">
		
		<div id="rd-topo-meio">
			
			<a class="a_logo" href="<?=$_SESSION["cid"]?>/index" title="<?=$v_title?>">
			<img src="imagizer_export.php?<?=Empresas::logomarca($_SESSION[$_SESSION["cid"]]["idempresa"])?>,185,95,2,0,,jpg" style="width: 185px; height: 95px;"/>
			</a>
			
			<div class="d_logado">
			<?
			if($_SESSION["cid"]=="usuario")
			{
				?>
				<a href="usuario/dados">
				Ol&aacute;&nbsp; <?=$_SESSION["usuario"]["nome"]; ?> </a><br />
				
				<a href="usuario/sair">
				Sair</a>
				<?
			}
			?>
			</div>            
            
			<div class="d_empresa">
			<?
			$d_empresas=Empresas::dados($_SESSION[$_SESSION["cid"]]["idempresa"]);
			
			if(count($d_empresas)>0)		
			{
			    ?>
			    <strong>
			    <!--
				<?=$d_empresas["rsocial"]?><br />
				-->
				<?
				if($d_empresas["site"]!="")
				{
					?>
					<a href="<?=$d_empresas["site"]?>" target="_blank">
			    	<?=$d_empresas["fantasia"]?></a><br />
			    	<?
    			}
    			else
    			{
    				?>
    				<?=$d_empresas["site"]?>
    				<?
    			}
    			?>
			    </strong>
			    <small>
			    <?=$d_empresas["endereco"]?> / <?=$d_empresas["bairro"]?><br />
                <?=$d_empresas["cidade"]?> - <?=$d_empresas["uf"]?> / <?=$d_empresas["cep"]?><br />
			    <?=$d_empresas["telefone"]?> / <?=$d_empresas["email"]?>
			    </small>
				<?
			}
			?>
			</div>
            
            <div class="d_menu">
            <?
            $d_menu=Central::menu();
            $d_menu_chaves=array_keys($d_menu);
			
			$pagina_superior="";
            
            for($i=0;$i<count($d_menu);$i++)
			{
				//var_dump($a_submenu);
				//
				$t_chave=$d_menu_chaves[$i];
				
				if(Central::submenu($t_chave,$pagina))
				{
					$pagina_superior=$t_chave;
				}
				
				$t_class=($t_chave==$pagina_superior OR $t_chave==$pagina)?"on":"";				
				?>
				<a class="<?=$t_class?>" href="<?=$_SESSION["cid"]?>/<?=$t_chave?>">
				<?=$d_menu[$t_chave][0]?></a>
				<?
			}            
            ?>
            </div>
            
            <div style="clear: both;"></div>
			
		</div> <!-- rd-cabecalho -->
	
	</div> <!-- rd-topo -->
	
	<div id="rd-conteudo">
		
		<table id="rd-conteudo-meio" style="width: 100%;" cellpadding="0" cellspacing="0">
		<tr valign="top">
			
			<?
			if($pagina_superior!="")
			{
				?>
				<td>
					<div class="d_submenu">
		            <?
		            $d_submenu=Central::submenu($pagina_superior);
		            $d_submenu_chaves=array_keys($d_submenu);
		            
		            for($i=0;$i<count($d_submenu);$i++)
					{
						$t_chave=$d_submenu_chaves[$i];
						
						$t_class=($t_chave==$pagina)?"on":"";
						?>
						<a class="<?=$t_class?>" href="<?=$_SESSION["cid"]?>/<?=$t_chave?>">
						<?=$d_submenu[$t_chave][0]?></a>
						<?
					}            
		            ?>				
					</div>
					
				</td>
				<?
			}
			?>
			
			<td style="width: 100%; border: 0px;">
				<div id="rd-pagina" class="<?=($pagina_superior!="")?"rd-pag-submenu":""?>">
				<?php
				include("{$pasta}/{$pagina}.php");
				flush();
				?>
				</div>
			</td>
		
		</tr>
		</table>
	
	</div> <!-- rd-conteudo -->	
	
	<div id="rd-rodape">
	
		<div id="rd-rodape-meio">
		
			<div class="d_rodape_copyright">
				
				<a class="a_desenvolvido">
				<img class="img_middle" src="design/desenv_vigo.png"/></a>
				
			</div>
		
		</div>
		
	</div> <!-- rd-rodape -->
	
</div> <!-- rd-geral -->	

<script type="text/javascript" defer="defer">
<?php
if($infomensagem!="")
{
	$infomensagem=str_replace("<br>","\\n",$infomensagem);
?>
    alert('<?= $infomensagem ?>');
<?php
}
?>
// console.log("Site carregado em: <?=round((microtime(true)-$tempo),3)?>");
</script>

</body>

</html>

