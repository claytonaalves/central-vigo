<?php 

// Testes de geracao de segunda-via do banco Unibanco

require_once('simpletest/autorun.php');
require_once('../mod_2via.php');

// TODO

class TestSegundaViaBoletosUnibanco extends UnitTestCase {

   function setUp() {
      $this->dados_boleto = array(
         'numbanco' => '004',
         'agencia'  => '1234',
         'conta'    => '00.056.789-0',
      );
   }


   function test01() {
   $this->dados_boleto['nnumero'] = '';
   $this->dados_boleto['valor'] = '0.00';
   $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2010-06-18'));

   $NovosDados = NovosDadosBoleto($this->dados_boleto);
   $this->assertEqual($NovosDados['codigobarras'], '');
   $this->assertEqual($NovosDados['linhadigitavel'], '');
   }

}   

?>