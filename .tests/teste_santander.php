<?php 

require_once('simpletest/autorun.php');
require_once('../mod_2via.php');

class TestSegundaViaBoletosSantander extends UnitTestCase {

   function setUp() {
      $this->dados_boleto = array(
         'numbanco' => '033',
         'agencia'  => '2130',
         'convenio'    => '2006081',
      );
   }


   function test01() {
      $this->dados_boleto['nnumero'] = '000000016541-7';
      $this->dados_boleto['valor'] = '56.30';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2011-11-07'));
      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '03392514400000056309200608100000001654170102');
      $this->assertEqual($NovosDados['linhadigitavel'], '03399.20068  08100.000002  16541.701021  2  51440000005630');
   }

   function test02() {
      $this->dados_boleto['nnumero'] = '000000016575-1';
      $this->dados_boleto['valor'] = '126.35';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2011-12-10'));
      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '03394517700000126359200608100000001657510102');
      $this->assertEqual($NovosDados['linhadigitavel'], '03399.20068  08100.000002  16575.101023  4  51770000012635');
   }

   function test03() {
      $this->dados_boleto['nnumero'] = '000000016594-8';
      $this->dados_boleto['valor'] = '88.67';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2012-01-18'));
      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '03397521600000088679200608100000001659480102');
      $this->assertEqual($NovosDados['linhadigitavel'], '03399.20068  08100.000002  16594.801025  7  52160000008867');
   }

}   

?>
