<?php 

// Testes de geracao de segunda-via da Caixa - Sinco

require_once('simpletest/autorun.php');
require_once('../mod_2via.php');

class TestSegundaViaBoletosSINCO extends UnitTestCase {

   function setUp() {
      $this->dados_boleto = array(
         'numbanco' => '104',
         'agencia'  => '1234',
         'conta'    => '00.065.432-1',
      );
   }


   function test01() {
      $this->dados_boleto['nnumero'] = '900000000000000001-4';
      $this->dados_boleto['valor'] = '79.90';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2010-06-22'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '10494464100000079901065432900000000000000001');
      $this->assertEqual($NovosDados['linhadigitavel'], '10491.06543  32900.000004  00000.000018  4  46410000007990');
   }


   function test02() {
      $this->dados_boleto['nnumero'] = '900000000000000002-2';
      $this->dados_boleto['valor'] = '79.90';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2010-07-22'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '10496467100000079901065432900000000000000002');
      $this->assertEqual($NovosDados['linhadigitavel'], '10491.06543  32900.000004  00000.000026  6  46710000007990');
   }


   function test03() {
      $this->dados_boleto['nnumero'] = '900000000000000004-9';
      $this->dados_boleto['valor'] = '79.90';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2010-09-22'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '10499473300000079901065432900000000000000004');
      $this->assertEqual($NovosDados['linhadigitavel'], '10491.06543  32900.000004  00000.000042  9  47330000007990');
   }


}   

?>