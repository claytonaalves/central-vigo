<?php 

require_once('simpletest/autorun.php');
require_once('../mod_2via.php');

// Testes com Bradesco carteira 09
class TestSegundaViaBoletosBradesco extends UnitTestCase {

   function setUp() {
      $this->dados_boleto = array(
         'numbanco' => '237',
         'agencia'  => '0523',
         'conta'    => '00.058.431-2',
         'convenio' => '09'
      );
   }


   function test01() {
      $this->dados_boleto['nnumero'] = '00000000131-P';
      $this->dados_boleto['valor'] = '351.00';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2010-01-26'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '23791449400000351000523090000000013100584310');
      $this->assertEqual($NovosDados['linhadigitavel'], '23790.52307  90000.000019  31005.843102  1  44940000035100');
   }


   function test02() {
      $this->dados_boleto['nnumero'] = '00000000811-P';
      $this->dados_boleto['valor'] = '290.00';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2010-03-01'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '23792452800000290000523090000000081100584310');
      $this->assertEqual($NovosDados['linhadigitavel'], '23790.52307  90000.000084  11005.843104  2  45280000029000');
   }


   function test03() {
      $this->dados_boleto['nnumero'] = '00000000795-4';
      $this->dados_boleto['valor'] = '290.00';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2010-02-24'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '23791452300000290000523090000000079500584310');
      $this->assertEqual($NovosDados['linhadigitavel'], '23790.52307  90000.000076  95005.843107  1  45230000029000');
   }
   
   
   function test04() {
      $dados_boleto = array(
         'numbanco' => '237',
         'agencia'  => '0523',
         'conta'    => '00.058.431-2',
         'convenio' => '09',
         'nnumero'  => '00000004909-6',
         'valor'    => '200.00',
         'vencimento' => date('d/m/Y', strtotime('2010-08-05'))
      );
      
      $NovosDados = NovosDadosBoleto($dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '23792468500000200000523090000000490900584310');
      $this->assertEqual($NovosDados['linhadigitavel'], '23790.52307  90000.000498  09005.843108  2  46850000020000');
   }
}   


// Testes para Bradesco carteira 06 (sem registro)
class TestSegundaViaBoletosBradesco_Carteira06 extends UnitTestCase 
{

   function setUp() 
   {
      $this->dados_boleto = array(
         'numbanco' => '237',
         'agencia'  => '3646-3',
         'conta'    => '00.013.460-0',
         'convenio' => '06'
      );
   }

   function test01()
   {
      $this->dados_boleto['nnumero'] = '00000030910-0';
      $this->dados_boleto['valor'] = '80.00';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2009-07-22'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '23796430600000080003646060000003091000134600');
      $this->assertEqual($NovosDados['linhadigitavel'], '23793.64603  60000.003099  10001.346005  6  43060000008000');
   }

   function test02()
   {
      $this->dados_boleto['nnumero'] = '00000023718-5';
      $this->dados_boleto['valor'] = '75.00';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2009-05-10'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '23797423300000075003646060000002371800134600');
      $this->assertEqual($NovosDados['linhadigitavel'], '23793.64603  60000.002372  18001.346008  7  42330000007500');
   }
}


// Testes para Bradesco carteira 06 (sem registro) com convenio
// cadastrado no vigo como 060000
class Test_2Via_Bradesco_Carteira06_2 extends UnitTestCase 
{

   function setUp() 
   {
      $this->dados_boleto = array(
         'numbanco' => '237',
         'agencia'  => '2038',
         'conta'    => '00.009.119-7',
         'convenio' => '060000'
      );
   }

   function test01()
   {
      $this->dados_boleto['nnumero'] = '00000074754-P';
      $this->dados_boleto['valor'] = '37.00';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2011-06-20'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '23798500400000037002038060000007475400091190');
      $this->assertEqual($NovosDados['linhadigitavel'], '23792.03801  60000.007470  54000.911906  8  50040000003700');
   }

}

?>
