<?php 

require_once('simpletest/autorun.php');
require_once('../mod_2via.php');

class TestSegundaViaBoletosSicoob extends UnitTestCase {

   function setUp() {
      $this->dados_boleto = array(
         'numbanco' => '756',
         'agencia'  => '3010',
         'conta'    => '00.028.372-0',
         'convenio' => '283720'
      );
   }


   function test01() {
      $this->dados_boleto['nnumero'] = '0000001-4';
      $this->dados_boleto['valor'] = '4.00';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2010-08-25'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '75691470500000004001301002028372000000014001');
      $this->assertEqual($NovosDados['linhadigitavel'], '75691.30102  02028.372007  00000.140012  1  47050000000400');
   }


   function test02() {
      $this->dados_boleto['nnumero'] = '0000002-1';
      $this->dados_boleto['valor'] = '3.00';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2010-08-25'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '75693470500000003001301002028372000000021001');
      $this->assertEqual($NovosDados['linhadigitavel'], '75691.30102  02028.372007  00000.210013  3  47050000000300');
   }


   function test03() {
      $this->dados_boleto['nnumero'] = '0000003-9';
      $this->dados_boleto['valor'] = '50.00';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2010-10-29'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '75692477000000050001301002028372000000039001');
      $this->assertEqual($NovosDados['linhadigitavel'], '75691.30102  02028.372007  00000.390013  2  47700000005000');
   }


   function test04() {
      $this->dados_boleto['nnumero'] = '0000008-5';
      $this->dados_boleto['valor'] = '50.00';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2011-03-28'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '75698492000000050001301002028372000000085001');
      $this->assertEqual($NovosDados['linhadigitavel'], '75691.30102  02028.372007  00000.850016  8  49200000005000');
   }
}   

?>

