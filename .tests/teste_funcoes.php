<?php 

require_once('simpletest/autorun.php');
require_once('../mod_2via.php');

class TestNovoVencimento extends UnitTestCase {

   function testPrimeiroJaneiro() {
      $data = NovoVencimento(strtotime('2010-01-01'));
      $this->assertEqual($data, '04/01/2010');
   }

   function test_23_janeiro() {
      $data = NovoVencimento(strtotime('2010-01-23'));
      $this->assertEqual($data, '25/01/2010');
   }

   function testDiaDoTrabalho() {
      $data = NovoVencimento(strtotime('2010-05-01'));
      $this->assertEqual($data, '03/05/2010');
   }

   function testNatal() {
      $data = NovoVencimento(strtotime('2010-12-25'));
      $this->assertEqual($data, '27/12/2010');
   }

   function testFinalDeAno() {
      $data = NovoVencimento(strtotime('2010-12-31'));
      $this->assertEqual($data, '31/12/2010');
   }

   function testPrimeiroDeJaneiro2011() {
      $data = NovoVencimento(strtotime('2011-01-01'));
      $this->assertEqual($data, '03/01/2011');
   }


}

// =========================================================

class TestDiasEmAtraso extends UnitTestCase {

   function testVencimentoSabado() {
      $vcto = '2010-05-22';
      $hoje = strtotime('2010-05-24');
      $this->assertEqual(DiasEmAtraso($vcto, $hoje), 0);
   }

   function testVencimentoSegunda() {
      $vcto = '2010-05-03';
      $hoje = strtotime('2010-05-14');
      $this->assertEqual(DiasEmAtraso($vcto, $hoje), 11);
   }
   
   function testFeriadoPrimeiroJaneiro() {
      $vcto = '2010-01-01';
      $hoje = strtotime('2010-01-04');
      $this->assertEqual(DiasEmAtraso($vcto, $hoje), 0);
   }

   function testVencimentoAposFeriado() {
      $vcto = '2010-01-02';
      $hoje = strtotime('2010-01-05');
      $this->assertEqual(DiasEmAtraso($vcto, $hoje), 1);
   }

   function testFeriadoTiradentes() {
      $vcto = '2010-04-21';
      $hoje = strtotime('2010-04-26');
      $this->assertEqual(DiasEmAtraso($vcto, $hoje), 4);
   }

   function testDoisDiasAntesDoNatal() {
      $vcto = '2010-12-23';
      $hoje = strtotime('2010-12-27');
      $this->assertEqual(DiasEmAtraso($vcto, $hoje), 4);
   }

}

?>