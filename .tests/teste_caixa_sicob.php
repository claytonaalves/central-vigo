<?php

require_once('simpletest/autorun.php');
require_once('../mod_2via.php');

class TestSegundaViaBoletosCaixaSicob extends UnitTestCase {


   function setUp() {
      $this->dados_boleto = array(
         'numbanco' => '104',
         'agencia'  => '0136',
         'conta'    => '00.000.115-7',
      );
   }


   function test01() {
      $this->dados_boleto['nnumero'] = '8200029313-4';
      $this->dados_boleto['valor'] = '88.00';
      $this->dados_boleto['vencimento'] = '12/06/2010';

      $boleto = new BoletoCaixa($this->dados_boleto);
      
      $this->assertEqual($boleto->codigo_barras, '10497463100000088008200029313013687000000115');
      $this->assertEqual($boleto->linha_digitavel, '10498.20002  29313.013681  70000.001159  7  46310000008800');
   }


   function test02() {
      $this->dados_boleto['nnumero'] = '8200029309-6';
      $this->dados_boleto['valor'] = '85.00';
      $this->dados_boleto['vencimento'] = '07/06/2010';

      $boleto = new BoletoCaixa($this->dados_boleto);
      
      $this->assertEqual($boleto->codigo_barras, '10496462600000085008200029309013687000000115');
      $this->assertEqual($boleto->linha_digitavel, '10498.20002  29309.013687  70000.001159  6  46260000008500');
   }


   function test03() {
      $this->dados_boleto['nnumero'] = '8200029311-8';
      $this->dados_boleto['valor'] = '60.00';
      $this->dados_boleto['vencimento'] = '07/06/2010';

      $boleto = new BoletoCaixa($this->dados_boleto);
      
      $this->assertEqual($boleto->codigo_barras, '10493462600000060008200029311013687000000115');
      $this->assertEqual($boleto->linha_digitavel, '10498.20002  29311.013683  70000.001159  3  46260000006000');
   }

}
 ?>