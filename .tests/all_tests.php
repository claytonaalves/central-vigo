<?php 
require_once('simpletest/autorun.php');

class AllTests extends TestSuite {

   function Alltests() {
      $this->TestSuite('All Tests');
      $this->addFile('teste_funcoes.php');
      
      // 001 - BANCO DO BRASIL
      // 01B - BANCO DO BRASIL CONV.7
      $this->addFile('teste_banco_do_brasil.php');   // Banco do Brasil (nnumero 11 e 17 posicoes)

      // 104 - CAIXA ECONÔMICA FEDERAL S.A.
      // 1X4 - CAIXA ECONÔMICA (SINCO)
      // 1Y4 - CAIXA ECONÔMICA (SIGCB)
      $this->addFile('teste_caixa_sicob.php');
      $this->addFile('teste_caixa_sigcb.php');
      $this->addFile('teste_caixa_sinco.php');

      // 356 - BANCO REAL
      $this->addFile('teste_banco_real.php');

      // 409 - UNIBANCO
      // $this->addFile('teste_unibanco.php');

      // 341 - BANCO ITAÚ S/A
      $this->addFile('teste_itau.php');

      // 237 - BRADESCO
      $this->addFile('teste_bradesco.php');

      // 099 - MULTIBANK
      $this->addFile('teste_multibank.php');
      
      // 748 - BANSICREDI
      $this->addFile('teste_bansicredi.php');
      
      // 399 - HSBC
      // $this->addFile('teste_hsbc.php');

      // 003 - BANCO DA AMAZÔNIA
      $this->addFile('teste_banco_amazonia.php');
      
      // 004 - BANCO DO NORDESTE
      $this->addFile('teste_banco_do_nordeste.php');
      
      // 756 - SICOOB
      $this->addFile('teste_sicoob.php');
  
      // 033 - SANTANDER
      $this->addFile('teste_santander.php');
   }

}

?>