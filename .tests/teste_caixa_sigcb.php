<?php 

// Testes de geracao de segunda-via da Caixa - Sigcb

require_once('simpletest/autorun.php');
require_once('../mod_2via.php');

class TestSegundaViaBoletosSIGCB extends UnitTestCase {

   function setUp() {
      $this->dados_boleto = array(
         'numbanco' => '104',
         'agencia'  => '0042',
         'conta'    => '00.086.315-7',
      );
   }


   function test01() {
      $this->dados_boleto['nnumero'] = '24000000000000001-2';
      $this->dados_boleto['valor'] = '3.00';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2009-01-20'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '10499412300000003000863157000200040000000010');
      $this->assertEqual($NovosDados['linhadigitavel'], '10490.86315  57000.200048  00000.000109  9  41230000000300');
   }


   function test02() {
      $this->dados_boleto['nnumero'] = '24000000000000004-7';
      $this->dados_boleto['valor'] = '45.00';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2009-02-12'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '10496414600000045000863157000200040000000045');
      $this->assertEqual($NovosDados['linhadigitavel'], '10490.86315  57000.200048  00000.000455  6  41460000004500');
   }


   function test03() {
      $this->dados_boleto['nnumero'] = '24000000000000116-7';
      $this->dados_boleto['valor'] = '45.00';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2009-02-13'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '10498414700000045000863157000200040000001165');
      $this->assertEqual($NovosDados['linhadigitavel'], '10490.86315  57000.200048  00000.011650  8  41470000004500');
   }


   function test04() {
      $this->dados_boleto['nnumero'] = '24000000000009664-8';
      $this->dados_boleto['valor'] = '65.00';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2009-12-10'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '10491444700000065000863157000200040000096646');
      $this->assertEqual($NovosDados['linhadigitavel'], '10490.86315  57000.200048  00000.966465  1  44470000006500');
   }


   function test05() {
      $this->dados_boleto['nnumero'] = '24000000000000115-9';
      $this->dados_boleto['valor'] = '45.00';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2009-02-13'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '10497414700000045000863157000200040000001157');
      $this->assertEqual($NovosDados['linhadigitavel'], '10490.86315  57000.200048  00000.011577  7  41470000004500');
   }

   function test06() {
      $this->dados_boleto['nnumero'] = '24000000000000013-6';
      $this->dados_boleto['valor'] = '250.00';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2009-02-12'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '10491414600000250000863157000200040000000134');
      $this->assertEqual($NovosDados['linhadigitavel'], '10490.86315  57000.200048  00000.001347  1  41460000025000');
   }

   function test07()
   {
      $dados_boleto = array(
         'numbanco' => '104',
         'agencia'  => '0948',
         // Essa conta estava cadastrada de forma incorreta
         //'conta'    => '00.903.760-0',
         'conta'    => '00.090.376-0',
      );
      $dados_boleto['nnumero'] = '24000000000016711-1';
      $dados_boleto['valor'] = '56.00';
      $dados_boleto['vencimento'] = date('d/m/Y', strtotime('2011-08-15'));

      $NovosDados = NovosDadosBoleto($dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '10491506000000056000903760000200040000167119');
      $this->assertEqual($NovosDados['linhadigitavel'], '10490.90374  60000.200042  00001.671197  1  50600000005600');
   }
}   

?>
