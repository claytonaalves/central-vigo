<?php 

require_once('simpletest/autorun.php');
require_once('../mod_2via.php');

class TestSegundaViaBoletosBB extends UnitTestCase {

   function setUp() {
      $this->dados_boleto = array(
         'numbanco' => '001',
         'agencia'  => '1821',
         'conta'    => '00.019.981-8'
      );
      // convenio: 283021
      // cod escritural: 0283021
   }


   function test01() {
      $this->dados_boleto['nnumero'] = '28302100001-3';
      $this->dados_boleto['valor'] = '55.00';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2010-06-22'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '00194464100000055002830210000118210001998118');
      $this->assertEqual($NovosDados['linhadigitavel'], '00192.83027  10000.118215  00019.981182  4  46410000005500');
   }


   function test02() {
      $this->dados_boleto['nnumero'] = '28302100002-1';
      $this->dados_boleto['valor'] = '65.00';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2010-06-23'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '00197464200000065002830210000218210001998118');
      $this->assertEqual($NovosDados['linhadigitavel'], '00192.83027  10000.218213  00019.981182  7  46420000006500');
   }


   function test03() {
      $this->dados_boleto['nnumero'] = '28302100003-X';
      $this->dados_boleto['valor'] = '79.90';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2010-12-31'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '00191483300000079902830210000318210001998118');
      $this->assertEqual($NovosDados['linhadigitavel'], '00192.83027  10000.318211  00019.981182  1  48330000007990');
   }


   function test04() {
      $this->dados_boleto['nnumero'] = '28302100004-8';
      $this->dados_boleto['valor'] = '79.90';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2011-01-30'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '00198486300000079902830210000418210001998118');
      $this->assertEqual($NovosDados['linhadigitavel'], '00192.83027  10000.418219  00019.981182  8  48630000007990');
   }

}   

// Banco do Brasil Convenio 7 digitos
class TestSegundaViaBoletosBB_Convenio7digitos extends UnitTestCase 
{

   function setUp() 
   {
      $this->dados_boleto = array(
         'numbanco' => '001',
         'agencia'  => '1599-7',
         'conta'    => '00.012.028-6',
         'convenio' => '1304910'
      );
   }
   
   function test01()
   {
      $this->dados_boleto['nnumero'] = '13049100000003739-1';
      $this->dados_boleto['valor'] = '51.50';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2010-07-10'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '00192465900000051500000001304910000000373918');
      $this->assertEqual($NovosDados['linhadigitavel'], '00190.00009  01304.910001  00003.739182  2  46590000005150');
   }

   function test02()
   {
      $this->dados_boleto['nnumero'] = '13049100000002362-5';
      $this->dados_boleto['valor'] = '1.00';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2009-05-18'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '00195424100000001000000001304910000000236218');
      $this->assertEqual($NovosDados['linhadigitavel'], '00190.00009  01304.910001  00002.362184  5  42410000000100');
   }

   function test03()
   {
      $this->dados_boleto['nnumero'] = '13049100000002361-7';
      $this->dados_boleto['valor'] = '51.50';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2009-09-10'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '00199435600000051500000001304910000000236118');
      $this->assertEqual($NovosDados['linhadigitavel'], '00190.00009  01304.910001  00002.361186  9  43560000005150');
   }

   function test04()
   {
      $this->dados_boleto['nnumero'] = '13049100000003234-9';
      $this->dados_boleto['valor'] = '51.50';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2010-09-10'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '00194472100000051500000001304910000000323418');
      $this->assertEqual($NovosDados['linhadigitavel'], '00190.00009  01304.910001  00003.234184  4  47210000005150');
   }
}

class TestVelbras extends UnitTestCase 
{

   function setUp() 
   {
      $this->dados_boleto = array(
         'numbanco'   => '001',
         'agencia'    => '2494-5',
         'conta'      => '00.011.660-2',
         'convenio'   => '1464435'
      );
   }
   
   function test01()
   {
      $this->dados_boleto['nnumero']    = '14644350000000002-X';
      $this->dados_boleto['valor']      = '73.50';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2008-11-10'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '00191405200000073500000001464435000000000218');
      $this->assertEqual($NovosDados['linhadigitavel'], '00190.00009  01464.435005  00000.002188  1  40520000007350');
   }
   
   function test02()
   {
      $this->dados_boleto['nnumero']    = '14644350000000008-9';
      $this->dados_boleto['valor']      = '73.50';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2008-11-10'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '00191405200000073500000001464435000000000818');
      $this->assertEqual($NovosDados['linhadigitavel'], '00190.00009  01464.435005  00000.008185  1  40520000007350');
   }
   
   function test03()
   {
      $this->dados_boleto['nnumero']    = '14644350000022287-1';
      $this->dados_boleto['valor']      = '153.50';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2011-05-10'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '00196496300000153500000001464435000002228718');
      $this->assertEqual($NovosDados['linhadigitavel'], '00190.00009  01464.435005  00022.287189  6  49630000015350');
   }

}

// 14644350000022290-1	10	    2011-04-10	00193493300000010000000001464435000002229018	00190.00009  01464.435005  00022.290183  3  49330000001000
// 14644350000022291-X	203.5	2011-05-10	00194496300000203500000001464435000002229118	00190.00009  01464.435005  00022.291181  4  49630000020350
// 14644350000022293-6	18.5	2011-05-09	00196496200000018500000001464435000002229318	00190.00009  01464.435005  00022.293187  6  49620000001850
// 14644350000022286-3	73.5	2011-05-10	00195496300000073500000001464435000002228618	00190.00009  01464.435005  00022.286181  5  49630000007350
// 14644350000022294-4	53.5	2011-05-10	00199496300000053500000001464435000002229418	00190.00009  01464.435005  00022.294185  9  49630000005350
// 14644350000022296-0	133.5	2011-03-10	00193490200000133500000001464435000002229618	00190.00009  01464.435005  00022.296180  3  49020000013350


?>
