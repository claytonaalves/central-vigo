<?php 

require_once('simpletest/autorun.php');
require_once('../mod_2via.php');

class TestSegundaViaBoletos extends UnitTestCase {

   function setUp() {
      $this->dados_boleto = array(
         'numbanco' => '341',
         'agencia'  => '1354',
         'conta'    => '00.021.354-6',
      );
   }
   
   function testSegundaViaComBancoNulo() {
      $this->dados_boleto['nnumero'] = '17500000001-6';
      $this->dados_boleto['valor'] = '50.00';
      $this->dados_boleto['numbanco'] = '347';
      
      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertNull($NovosDados);
   }

   function test01() {
      $this->dados_boleto['nnumero'] = '17500000001-6';
      $this->dados_boleto['valor'] = '50.00';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2010-01-01'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '34194446900000050001750000000161354213546000');
      $this->assertEqual($NovosDados['linhadigitavel'], '34191.75009  00000.161356  42135.460006  4  44690000005000');
   }

   function test02() {
      $this->dados_boleto['nnumero'] = '17500000002-4';
      $this->dados_boleto['valor'] = '50.00';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2010-02-01'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '34197450000000050001750000000241354213546000');
      $this->assertEqual($NovosDados['linhadigitavel'], '34191.75009  00000.241356  42135.460006  7  45000000005000');
   }
   
   function test03() {
      $this->dados_boleto['nnumero'] = '17500000009-9';
      $this->dados_boleto['valor'] = '75.99';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2010-03-01'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '34196452800000075991750000000991354213546000');
      $this->assertEqual($NovosDados['linhadigitavel'], '34191.75009  00000.991356  42135.460006  6  45280000007599');
   }
}   

?>