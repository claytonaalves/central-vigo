<?php 

// Testes de geracao de segunda-via do banco Bansicredi

require_once('simpletest/autorun.php');
require_once('../mod_2via.php');

class TestSegundaViaBoletosBansicredi extends UnitTestCase {

   function setUp() {
      $this->dados_boleto = array(
         'numbanco' => '748',
         'agencia'  => '0818-0',
         'conta'    => '00.009.899-0',
         'convenio' => '13'
      );
   }


   function test01() {
      $this->dados_boleto['nnumero'] = '0818130989910228977-1';
      $this->dados_boleto['valor'] = '22.65';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2010-08-20'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '74895470000000022653110228977108181309899108');
      $this->assertEqual($NovosDados['linhadigitavel'], '74893.11022  28977.108183  13098.991089  5  47000000002265');
   }


   function test02() {
      $this->dados_boleto['nnumero'] = '0818130989910228978-0';
      $this->dados_boleto['valor'] = '40.00';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2010-09-20'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '74899473100000040003110228978008181309899107');
      $this->assertEqual($NovosDados['linhadigitavel'], '74893.11022  28978.008184  13098.991071  9  47310000004000');
   }


   function test03() {
      $this->dados_boleto['nnumero'] = '0818130989910228979-8';
      $this->dados_boleto['valor'] = '40.00';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2010-08-13'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '74893469300000040003110228979808181309899109');
      $this->assertEqual($NovosDados['linhadigitavel'], '74893.11022  28979.808186  13098.991097  3  46930000004000');
   }

   function test04() {
      $boleto = array( 'numbanco' => '748',
                       'agencia'  => '0710',
                       'conta'    => '00.064.824-8',
                       'convenio' => '42' );

      $boleto['nnumero'] = '0710426482409201965-4';
      $boleto['valor'] = '35.00';
      $boleto['vencimento'] = date('d/m/Y', strtotime('2009-10-10'));

      $NovosDados = NovosDadosBoleto($boleto);
      $this->assertEqual($NovosDados['codigobarras'], '74894438600000035003109201965407104264824102');
      $this->assertEqual($NovosDados['linhadigitavel'], '74893.10925  01965.407107  42648.241026  4  43860000003500');
   }

}   

?>