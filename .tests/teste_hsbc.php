<?php 

require_once('simpletest/autorun.php');
require_once('../mod_2via.php');

// 399 - HSBC
class TestSegundaViaBoletosHSBC extends UnitTestCase {

   function setUp() {
      $this->dados_boleto = array(
         'numbanco' => '399',
         'agencia'  => '0077',
         'conta'    => '04.003.233-0',
         'convenio' => '4003233'
      );
   }

   function test01() {
      $this->dados_boleto['nnumero'] = '00000001945';
      $this->dados_boleto['valor'] = '15.90';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2010-09-10'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '39999472100000015904003233000000000000125302');
      $this->assertEqual($NovosDados['linhadigitavel'], '39994.00322  33000.000001  00001.253020  9  47210000001590');
   }

   function test02() {
      $this->dados_boleto['nnumero'] = '00000002747';
      $this->dados_boleto['valor'] = '15.90';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2010-09-10'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '39992472100000015904003233000000000000225302');
      $this->assertEqual($NovosDados['linhadigitavel'], '39994.00322  33000.000001  00002.253029  2  47210000001590');
   }

   function test03() {
      $this->dados_boleto['nnumero'] = '00000003549';
      $this->dados_boleto['valor'] = '15.90';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2010-09-10'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '39996472100000015904003233000000000000325302');
      $this->assertEqual($NovosDados['linhadigitavel'], '39994.00322  33000.000001  00003.253028  6  47210000001590');
   }

}   

// | 00000006041 |   15.9 | 2010-09-10 | 39997472100000015904003233000000000000625302 | 39994.00322  33000.000001  00006.253025  7  47210000001590 |
// | 00000007846 | 105.34 | 2010-09-10 | 39991472100000105344003233000000000000725302 | 39994.00322  33000.000001  00007.253024  1  47210000010534 |
// | 00000004340 |   15.9 | 2010-09-10 | 39991472100000015904003233000000000000425302 | 39994.00322  33000.000001  00004.253027  1  47210000001590 |
// | 00000005142 |   15.9 | 2010-09-10 | 39993472100000015904003233000000000000525302 | 39994.00322  33000.000001  00005.253026  3  47210000001590 |
// | 00000008648 |   15.9 | 2010-09-10 | 39994472100000015904003233000000000000825302 | 39994.00322  33000.000001  00008.253023  4  47210000001590 |

?>