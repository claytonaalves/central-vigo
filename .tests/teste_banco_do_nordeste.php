<?php 

require_once('simpletest/autorun.php');
require_once('../mod_2via.php');

class TestSegundaViaBoletosBNB extends UnitTestCase {

   function setUp() {
      $this->dados_boleto = array(
         'numbanco' => '004',
         'agencia'  => '0134',
         'conta'    => '00.006.638-3',
      );
   }


   function test01() {
      $this->dados_boleto['nnumero'] = '0000001-9';
      $this->dados_boleto['valor'] = '30.00';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2010-06-16'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '00496463500000030000134000663830000001951000');
      $this->assertEqual($NovosDados['linhadigitavel'], '00490.13402  00663.830008  00019.510007  6  46350000003000');
   }


   function test02() {
      $this->dados_boleto['nnumero'] = '0000002-7';
      $this->dados_boleto['valor'] = '27.50';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2010-06-17'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '00492463600000027500134000663830000002751000');
      $this->assertEqual($NovosDados['linhadigitavel'], '00490.13402  00663.830008  00027.510007  2  46360000002750');
   }


   function test03() {
      $this->dados_boleto['nnumero'] = '0000003-5';
      $this->dados_boleto['valor'] = '19.90';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2010-06-21'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '00491464000000019900134000663830000003551000');
      $this->assertEqual($NovosDados['linhadigitavel'], '00490.13402  00663.830008  00035.510007  1  46400000001990');
   }


   function test04() {
      $this->dados_boleto['nnumero'] = '0000004-3';
      $this->dados_boleto['valor'] = '5.58';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2010-06-20'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '00498463900000005580134000663830000004351000');
      $this->assertEqual($NovosDados['linhadigitavel'], '00490.13402  00663.830008  00043.510007  8  46390000000558');
   }
}   

?>