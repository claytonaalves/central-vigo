<?php 

require_once('simpletest/autorun.php');
require_once('../mod_2via.php');

// Testes com Banco Real
class TestSegundaViaBoletosBancoReal extends UnitTestCase {

   function setUp() {
      $this->dados_boleto = array(
         'numbanco' => '356',
         'agencia'  => '0962',
         'conta'    => '09.011.824-6',
         'convenio' => '57'
      );
   }

   function test01() {
      $this->dados_boleto['nnumero'] = '0000000000052';
      $this->dados_boleto['valor'] = '290.00';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2009-07-20'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '35695430400000290000962901182440000000000052');
      $this->assertEqual($NovosDados['linhadigitavel'], '35690.96294  01182.440006  00000.000521  5  43040000029000');
   }
   
   function test02() {
      $this->dados_boleto['nnumero'] = '0000000000053';
      $this->dados_boleto['valor'] = '110.00';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2009-07-10'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '35697429400000110000962901182430000000000053');
      $this->assertEqual($NovosDados['linhadigitavel'], '35690.96294  01182.430007  00000.000539  7  42940000011000');
   }
   
   function test03() {
      $this->dados_boleto['nnumero'] = '0000000000436';
      $this->dados_boleto['valor'] = '49.90';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2009-07-18'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '35693430200000049900962901182410000000000436');
      $this->assertEqual($NovosDados['linhadigitavel'], '35690.96294  01182.410009  00000.004366  3  43020000004990');
   }
   
   function test04() {
      $this->dados_boleto['nnumero'] = '0000000000045';
      $this->dados_boleto['valor'] = '120.00';
      $this->dados_boleto['vencimento'] = date('d/m/Y', strtotime('2009-07-15'));

      $NovosDados = NovosDadosBoleto($this->dados_boleto);
      $this->assertEqual($NovosDados['codigobarras'], '35691429900000120000962901182440000000000045');
      $this->assertEqual($NovosDados['linhadigitavel'], '35690.96294  01182.440006  00000.000455  1  42990000012000');
   }
   
}   

?>