<?php

/**
 * @author VitorGGA
 * @copyright 2007
 * @version 20070422
 */

function autoform($dados)
{
	// 0 - text - char,int
	// 1 - textarea - char,array(int,int)
	// 2 - checkbox - char,array(text*)
	// 3 - radio - char,array(text*)
	// 4 - select - char,array(text*)
	// 5 - hr - char
	
	for($i=0;$i<count($dados);$i++)
	{
		?><div id="autoform"><?php
		switch($dados[$i][0])
		{
			//TEXT
			case 0:
			{
				?>
				<span id="af_titulo"><?=$dados[$i][1]?></span>
				<span id="af_campos">
				<input type="text" name="<?=$dados[$i][1]?>" value="" size="<?=$dados[$i][2]?>">
				</span>
				<?php				
			}
			break;
			//TEXTAREA
			case 1:
			{
				?>
				<span id="af_titulo"><?=$dados[$i][1]?></span>
				<span id="af_campos">
				<textarea name="<?=$dados[$i][1]?>" rows="<?=$dados[$i][2][0]?>" cols="<?=$dados[$i][2][1]?>"></textarea>
				</span>
				<?php				
			}
			break;
			//CHECKBOX
			case 2:
			{
				?>
				<span id="af_titulo"><?=$dados[$i][1]?></span>
				<span id="af_campos">
				<?php
				for($c=0;$c<count($dados[$i][2]);$c++)
				{
					?>
					<span><input id="<?=$i.$c?>" name="<?=$dados[$i][1]?> - <?=$dados[$i][2][$c]?>" type="checkbox" value="Marcado"><label for="<?=$i.$c?>"><?=$dados[$i][2][$c]?></label></span>
					<?php
				}
				?>
				</span>
				<?php				
			}
			break;
			//RADIO
			case 3:
			{
				?>
				<span id="af_titulo"><?=$dados[$i][1]?></span>
				<span id="af_campos">
				<?php
				for($c=0;$c<count($dados[$i][2]);$c++)
				{
					?>
					<span><input id="<?=$i.$c?>" name="<?=$dados[$i][1]?>" type="radio" value="<?=$dados[$i][2][$c]?>"><label for="<?=$i.$c?>"><?=$dados[$i][2][$c]?></label></span>
					<?php
				}
				?>
				</span>
				<?php				
			}
			break;
			//SELECT
			case 4:
			{
				?>
				<span id="af_titulo"><?=$dados[$i][1]?></span>
				<span id="af_campos">
				<select name="<?=$dados[$i][1]?>">
				<?php
				for($c=0;$c<count($dados[$i][2]);$c++)
				{
					?>
					<option><?=$dados[$i][2][$c]?></option>
					<?php
				}
				?>
				</select>
				</span>
				<?php				
			}
			break;
			//HR
			case 5:
			{
				?>
				<span id="af_titulo"><hr><b><?=$dados[$i][1]?></b><hr></span>
				<span id="af_campos">
				<input type="hidden" name="_<?=$dados[$i][1]?>">
				</span>
				<?php				
			}
			break;
			//NENHUM
			default:
			{
				?>
				<?=$dados[$i][1]?>
				<?php
			}
			break;
		}
		?></div><?php
	}
	
	//print_r($dados);
}

?>