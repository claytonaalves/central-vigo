<?php
ob_clean();

function esquerda($entra,$comp){
   return substr($entra,0,$comp);
}

function direita($entra,$comp){
   return substr($entra,strlen($entra)-$comp,$comp);
}

function montacodigodebarras($valor) {
   $lw = 1; $hi = 50;

   $tabcodbarra[0] = "00110";
   $tabcodbarra[1] = "10001";
   $tabcodbarra[2] = "01001";
   $tabcodbarra[3] = "11000";
   $tabcodbarra[4] = "00101";
   $tabcodbarra[5] = "10100";
   $tabcodbarra[6] = "01100";
   $tabcodbarra[7] = "00011";
   $tabcodbarra[8] = "10010";
   $tabcodbarra[9] = "01010";
   
   for ($f1 = 9; $f1 >= 0; $f1--) {
      for ($f2 = 9; $f2 >= 0; $f2--) {  
         $f = ($f1 * 10) + $f2;
         $texto = "";
         for ($i = 1; $i < 6; $i++) {
            $texto .=  substr($tabcodbarra[$f1],($i-1),1) . substr($tabcodbarra[$f2],($i-1),1);
         }
         $tabcodbarra[$f] = $texto;
      }
   }
     
   //~ $img = ImageCreate($lw*95+1000,$hi+30);
   $img = ImageCreate(405,60);
   $preto  = ImageColorAllocate($img, 0, 0, 0);
   $branco = ImageColorAllocate($img, 255, 255, 255);
      
   ImageFilledRectangle($img, 0, 0, 405, 60, $branco); // fundo branco
   ImageFilledRectangle($img, 0, 0, 0, 60, $preto);
   ImageFilledRectangle($img, 1, 0, 1, 60, $branco);
   ImageFilledRectangle($img, 2, 0, 2, 60, $preto);
   ImageFilledRectangle($img, 3, 0, 3, 60, $branco);

   $fino  = 1;
   $largo = 3;
   $pos   = 4;
   $texto = $valor;
   
   if ((strlen($texto) % 2) <> 0)
      $texto = "0" . $texto;
   
   while (strlen($texto) > 0) {
      $i = round(esquerda($texto,2));
      $texto = direita($texto,strlen($texto)-2);

      $f = $tabcodbarra[$i];

      for ( $i = 1; $i < 11; $i += 2 ) {
         if (substr($f,($i-1),1) == "0")
            $f1 = $fino;
         else
            $f1 = $largo;

         ImageFilledRectangle($img, $pos, 0, $pos+$f1, 60, $preto) ;
         $pos = $pos + $f1;  

         if (substr($f,$i,1) == "0")
            $f2 = $fino;
         else
            $f2 = $largo;
            
         ImageFilledRectangle($img, $pos, 0, $pos+$f2, 60, $branco) ;
         $pos = $pos + $f2;  
      }
   }

   ImageFilledRectangle($img, $pos,0,$pos-1+$largo,60,$preto);
   $pos=$pos+$largo;
   ImageFilledRectangle($img, $pos,0,$pos-1+$fino,60,$branco);
   $pos=$pos+$fino;
   ImageFilledRectangle($img, $pos,0,$pos-1+$fino,60,$preto);
   $pos=$pos+$fino;

   header("Content-Type: image/png");
   ImagePNG($img);
}

montacodigodebarras($_GET['numero']);

exit();
?>