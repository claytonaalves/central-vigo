<?php

// Lista o extrato de horas de conexoes via radius

// Vitor, depois coloca isso aqui onde vc achar mais conveniente
$tipos_de_desconexao = array(
    'User-Request' => 'Solicitada pelo usu�rio.',
    'Lost-Service' => 'Conex�o interrompida.',
    'Idle-Timeout' => 'Expirado por falta de atividade na rede.',
    'Admin-Reset' => 'Resetada pelo administrador.',
    'Session-Timeout' => 'Tempo de conex�o esgotado.',
    'Admin-Reboot' => 'Reboot do administrador.'
);

global $mikrotik;
$mikrotik=Conexao::conn("mikrotik");

$mk_login = mysql_query("SELECT DISTINCT username FROM radcheck WHERE id_cliente='{$_SESSION["usuario"]["id"]}' AND username NOT LIKE '%:%:%:%:%:%' ORDER BY username", $mikrotik);
$totalRows_mk_login = mysql_num_rows($mk_login);

if ($acao=="listar" OR $acao=="grafico") {
   
   $login = mysql_real_escape_string($_POST['login']);
   
   //$inicio = substr($_POST['dt_inicio'], 6, 4)."-".substr($_POST['dt_inicio'], 3, 2)."-".substr($_POST['dt_inicio'], 0, 2)." ".substr($_POST['dt_inicio'], 11, 8);
   //$fim = substr($_POST['dt_final'], 6, 4)."-".substr($_POST['dt_final'], 3, 2)."-".substr($_POST['dt_final'], 0, 2)." ".substr($_POST['dt_final'], 11, 8);;
	$inicio=Suporte::datar($dt_inicio);
	$fim=Suporte::datar($dt_final);

   // Seleciona a lista de conexoes de radacct
   $r_horas = mysql_query("SELECT * FROM radacct 
                           WHERE (UserName='$login') AND 
                                 (AcctStartTime BETWEEN '$inicio' AND '$fim') AND 
                                 ((AcctStopTime<>'0000-00-00 00:00:00') OR (AcctStopTime IS NOT NULL)) 
                           ORDER by AcctStartTime ASC", $mikrotik);
   //$row_horas = mysql_fetch_assoc($r_horas);
   //$totalRows_horas = mysql_num_rows($horas);
   
   $sql="SELECT DISTINCT ug.username, ug.groupname, radgroupreply.attribute, radgroupreply.value FROM radcheck LEFT JOIN usergroup ug ON radcheck.username=ug.username LEFT JOIN radgroupreply using(groupname) WHERE radgroupreply.attribute='Mikrotik-Rate-Limit' AND ug.username='{$login}'";
   
   $r_plano = mysql_query($sql, $mikrotik);
   
}
else
{
	$login = '';
}

$dt_inicio=($dt_inicio=="")?date("d/m/Y H:i",strtotime("-30 day")):$dt_inicio;
$dt_final=($dt_final=="")?date("d/m/Y H:i"):$dt_final;

?>
<h1>Extrato de horas de usu&aacute;rios</h1>

<p>
<b>Aviso Legal</b><br />
Esse extrato &eacute; para consulta particular do cliente e a utiliza&ccedil;&atilde;o do
mesmo   para qualquer outra finalidade ser&aacute; de responsabilidade exclusiva do cliente.
Caso voc&ecirc; verifique a utiliza&ccedil;&atilde;o indevida de sua senha, solicitamos que
voc&ecirc;   providencie a altera&ccedil;&atilde;o   imediata da mesma ou entre em contato com o
Provedor de Acesso.
</p>

<form class="f_filtro" action="" method="post">
<input type="hidden" name="acao" value="listar"/>
    
	<span>
	<label>Login de acesso</label>
	<select name="login" id="login">
	<?php
	while ($row_mk_login = mysql_fetch_assoc($mk_login))
	{
		print "<option value='".$row_mk_login['username']."'>".$row_mk_login['username']."</option>";
	}
	?>
	</select>		
	</span>
	
	<span>
	<label>Inicio</label>
	<input class="c_data_hora" name="dt_inicio" type="text" class="myinputstyle" style="myinputstyle" value="<?=$dt_inicio?>" size="20"/>
    </span>
	
	<span>
	<label>Fim</label>
	<input class="c_data_hora" name="dt_final" type="text" class="myinputstyle" value="<?=$dt_final?>" size="20"/>
	</span>
    
    <span>
	<button type="submit">
	Fazer Consulta</button>
	</span>
    
</form>

<?php
if($acao=="listar")
{
	
	if(mysql_num_rows($r_plano)>0)
	{
		$d_plano=mysql_fetch_array($r_plano);
		
		$t_limite=$d_plano["value"];
		$t_limite=strstr($t_limite,"}'");
		$t_limite=Suporte::onlyNumber($t_limite);
		$t_limite=$t_limite/1024;
		
		?>
		<div class="f_cadastro">
			
			<span>
			<label>Usu�rio:</label>
			<?=$d_plano["username"]?>
			</span>
			
			<span>
			<label>Plano:</label>
			<?=str_replace('(burst)', '', $d_plano["groupname"])?>
			</span>
			
			<?
			if($t_limite>0)
			{
				?>
				<span>
				<label>Limite:</label>
				<?=round($t_limite,2)?> Kb
				</span>
				<?
			}
			?>
			
		</div>
		<?
	}
	
	if(mysql_num_rows($r_horas)==0)
	{
		?>
	  	<p>
		N&atilde;o foi encontrado nenhuma conex&atilde;o neste per&iacute;odo.
		</p>
	  	<?php
	}
	?>
	<table class="t_listagem" style="width: 100%;" cellpadding="5" cellspacing="0">
	<thead>
	<tr>
		<td>Inicio</td>
		<td>Fim</td>
		<td style="text-align: right;">Upload</td>
		<td style="text-align: right;">Download</td>
		<td style="text-align: right;">Detalhes</td>
	</tr>
	</thead>
	<tbody>
	<?php
	$total_upload=0;
	$total_download=0;
	$total_tempo=0;
	
	for($i=0;$i<mysql_num_rows($r_horas);$i++)
	{
		$row_horas=mysql_fetch_array($r_horas);
		$t_tempo_start=strtotime($row_horas['AcctStartTime']);
		$t_tempo_stop=strtotime($row_horas['AcctStopTime']);
		
		$t_upload=floatval($row_horas['AcctInputOctets']/1024/1024);
		$t_download=floatval($row_horas['AcctOutputOctets']/1024/1024);
		
		$total_upload+=$t_upload;
		$total_download+=$t_download;
		
		$total_tempo+=strtotime($row_horas['AcctStopTime'])-strtotime($row_horas['AcctStartTime']);
		
		?>
		<tr>
			
			<td>
			<?php echo date("d/m/Y H:i:s",strtotime($row_horas['AcctStartTime'])); ?>
			</td>
			
			<td>
			<?php echo date("d/m/Y H:i:s",strtotime($row_horas['AcctStopTime'])); ?>
			</td>
			
			<td style="text-align: right;">
			<?=round($t_upload,2)?> MB
			</td>
			
			<td style="text-align: right;">
			<?=round($t_download,2)?> MB
			</td>
			
			<td style="width: 30%; text-align: right;">
				
				<a class="lytediv" href="#info_<?php echo $row_horas['radacctid']?>">
				<img class="mm" src="imagens/icones/info.gif"/>
				Detalhes do Acesso</a>
				
				<!-- AQUI IREMOS COLOCAR AS MENSAGENS -->   
				
				<div id="info_<?php echo $row_horas['radacctid']; ?>" style="display:none">
				
					<div class="f_cadastro">
					
						<h1>Detalhes do acesso</h1>
						
						<span>
						<label>Login:</label>
						<?php echo $row_horas['username']; ?>
						</span>
						
						<span>
						<label>Inicio da conex�o:</label>
						<?php echo Suporte::datar($row_horas['AcctStartTime']); ?>
						</span>
						
						<span>
						<label>Fim da Conex�o:</label>
						<?php echo Suporte::datar($row_horas['AcctStopTime']); ?>
						</span>
						
						<span>
						<label>Dura��o:</label>
						<?php
						echo Suporte::datar_humano($row_horas['AcctStartTime'],$row_horas['AcctStopTime']);
						?>
						</span>
						
						<span>
						<label>Endere�o IP:</label>
						<?php echo $row_horas['framedipaddress']; ?>
						</span>
						
						<span>
						<label>Endere�o MAC:</label>
						<?php echo $row_horas['callingstationid']; ?>
						</span>
						
						<span>
						<label>Motivo da desconex�o:</label>
						<?php echo $tipos_de_desconexao[$row_horas['acctterminatecause']]; ?>
						</span>
						
					</div>
					
				</div>
				
				<!-- FIM DAS MENSAGENS -->
			
			</td> 
		
		</tr>
		<?php
	} 
	?>
	<tfoot>
	<tr>
		
		<td colspan="2">
		<?
		$a_total_tempo=Suporte::dataRestante(0,$total_tempo);
		?>
		Total: <?=$a_total_tempo[0]?> dias <?=$a_total_tempo[1]?> horas <?=$a_total_tempo[2]?> minutos
		</td>
		
		<td style="text-align: right;">
		Total: <?=round($total_upload,2)?> MB
		</td>
		
		<td style="text-align: right;">
		Total: <?=round($total_download,2)?> MB
		</td>
		
		<td>
		</td>
		
	</tr>
	</tfoot>
	</tbody>
	</table>
	<?
}
?>
