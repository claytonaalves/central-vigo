<?php
//classe para upload de imagem

class Uploadjpg{
	var $erro;
	var $imgy;
	var $tamanho;
	var $nometmp;
	var $nome;
	var $tipo;
	var $quali;
	var $novonome;
	var $diretorio;
	
	function Uploadjpg(){
    }
	//recebe
	function Recebe($imagem){	
		$this->tamanho = $imagem['size'];
		$this->nometmp = $imagem['tmp_name'];
		$this->tipo = $imagem['type'];
		$this->nome = $imagem['name'];
	}
	//gera imagem
	function Gera($imgy,$quali,$diretorio){
		$this->imgy = $imgy;
		$this->quali = $quali;
		$this->diretorio = $diretorio;
			
		if (($this->tipo != 'image/jpeg') && ($this->tipo != 'image/pjpeg')){
			$this->erro =  'Tipo de arquivo invlido, somente JPG.';
			return false;	
		}elseif($this->tamanho == 0){
			$this->erro =  'Selecione uma imagem.';
			return false;
		}elseif(!$imagem_orig = @imagecreatefromjpeg($this->nometmp)){
			$this->erro =  'Imagem JPG com formato incompativel.';
			return false;
		}elseif(!file_exists($this->diretorio)){
			$this->erro =  'O diretorio '.$this->diretorio.' nao existe.';
			return false;
		}else{
			//gera a imagem
			$imagem = $this->diretorio.$this->novonome; 
			$tamanhoX = @imagesx($imagem_orig);
			$tamanhoY = @imagesy($imagem_orig); 
			$altura = $this->imgy; 
			//A linha abaixo � para largura proporcional da altura
			//$largura = $tamanhoX * $altura / $tamanhoY; 
			$largura = 194; 
			$imagem_fin = imagecreatetruecolor($largura, $altura); 
			imagecopyresampled($imagem_fin,$imagem_orig, 0, 0, 0, 0, $largura + 1, $altura + 1, $tamanhoX, $tamanhoY); 
			imageJPEG($imagem_fin,$imagem,$this->quali);
			@imagedestroy($imagem_orig);
			@imagedestroy($imagem_fin);
			//se deu certo retorna o nome da imagem
			return $this->novonome;
		}
	}
	
	//retorna erro
	function Erro(){
		return $this->erro;
	}
	//seta nome
	function Setanome($nome){
		$this->novonome = $nome;
	}
	//cria diretorio
	function Criadir($novodir,$chmod){
		if(file_exists($novodir)){
			return false;
		}elseif(!@mkdir($novodir,$chmod)){
			return false;
		}else{
			return true;
		}
	}
}
?>

<?php
/* Upload
require_once "class.uploadjpg.php";

//faz o upload da foto
$diretorio = 'imagens/fotos/';
$envia = new Uploadjpg;
$envia->Recebe($_FILES['foto']);
$envia->Setanome(date('dmYhis').'.jpg');
$foto = $envia->Gera(182,100,$diretorio);

if(!$foto){
		$erro = $envia->Erro();
}else{
SQL
}
*/
?>
