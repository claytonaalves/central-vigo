<?
header("Content-Type: text/html; charset=iso-8859-1");

if($_SESSION["chat"]=="")
{
	//exit(header("Location: {$v_base}usuario/chat_iniciar"));
	include("chat_iniciar.php");
}
else
{
	$d_chat=Chat::dados($_SESSION["chat"]);
	
	if($acao=="postar")
	{
		ob_clean();
		
		$a_dados=array();
		$a_dados["mensagem"]=utf8_decode($mensagem);
		$a_dados["usuario"]=$_SESSION["usuario"]["numero"];
		$a_dados["operador"]="";
		
		Chat::postar($_SESSION["chat"],$a_dados);
		
		exit();
	}
	
	if($acao=="mensagens")
	{
		ob_clean();
		
		Chat::mensagens($_SESSION["chat"]);
		
		exit();
	}
	
	if($acao=="status")
	{
		ob_clean();
	
		// Ping usu�rio
	
		Chat::ping($_SESSION["chat"],"usuario");
		
		// Busca id ultima mensagem
		
		$ultimaMsg=Chat::mensagensIdUltima($_SESSION["chat"]);
	
		?>
		msgUltima='<?=$ultimaMsg?>';
		<?
	
		// Verifica status
	
		$d_chat=Chat::dados($_SESSION["chat"]);
		
		?>
		msgStatus='<?=$d_chat["status"]?>';
		<?
		
		if($d_chat["status"]==2)
		{
			?>
			$("#d_status").html('Status: OK');
			<?
		}
		
		if($d_chat["status"]==1)
		{
			?>
			$("#d_status").html('Status: Finalizado');
			fecharChat();
			<?
		}
		
		// Checa ausencia
		
		$t_ausencia=Chat::checarAusencia($_SESSION["chat"]);
		
		if($t_ausencia=="operador")
		{
			?>
			$("#d_status").html('N�o h� nenhum operador dispon�vel para atendimento.');
			<?
			exit();
		}
		
		exit();
	}
	
	if($acao=="fechar")
	{
		ob_clean();
		
		$a_dados=array();
		$a_dados["mensagem"]="* Usuario fechou chat!";
		$a_dados["usuario"]="";
		$a_dados["operador"]="";
		$a_dados["info"]="1";
		
		Chat::postar($_SESSION["chat"],$a_dados);
		
		Chat::fechar($_SESSION["chat"]);
		
		exit();
	}
	
	if($d_chat["status"]==0)
	{
		exit(header("Location: {$v_base}usuario/chat_fila"));
	}
	
	if($d_chat["status"]==1)
	{
		exit(header("Location: {$v_base}usuario/chat_finalizado"));
	}
	
	?>
	<link rel="stylesheet" type="text/css" href="css_chat.css"/>
	
	<script type="text/javascript">
	
	function enviarMsg()
	{
		msg=$("#mensagem").val();
		$("#mensagem").val("");
		
		if(msg=="")
		{
			return(false);
		}
		
		$.ajax({  
			type: "POST",  
			url: "usuario/chat/?acao=postar",  
			data: "mensagem="+msg,
			async: false,
			success: function(retorno) {  
				  atualizarMsg();
				  msgRolagem(true);
			}  
		});
		
		return(false);
	}
	
	msgErros=0;
	msgStatus=0;
	msgAtual="";
	msgUltima="";
	msgFechando=false;
	
	function atualizarMsg()
	{
		$("#d_status").html("Checando...");
	
		if(msgErros>30)
		{
			alert("Conex�o caiu!");
		}
		
		$.ajax({  
	 		url: "usuario/chat/?acao=mensagens",  
			success: function(retorno) {  
				$("#d_mensagens div").html(retorno);
				$("#d_status").html("Atualizado.");
				msgErros=0;
				msgRolagem();
				msgAtual=msgUltima;
				
				$(window).blur(function(){
				    //$(window).focus();
				});			
				
			},
			error: function() {
				msgErros++;
			}
		});
	
	}
	
	function msgRolagem(paraFinal)
	{
		var psconsole = $('#d_mensagens');
		
		t_scroll=psconsole[0].scrollHeight - psconsole.height();
		
		if( psconsole.scrollTop()>(t_scroll-50) || paraFinal==true )
		{
			psconsole.scrollTop(t_scroll);
		}
		
	}
	
	function checarStatus()
	{
		$.ajax({  
	 		url: "usuario/chat/?acao=status",  
			success: function(retorno) {  
				eval(retorno);
				
				if(msgAtual!=msgUltima)
				{
					atualizarMsg();
				}
				
				if(msgStatus=='1')
				{
					$("#frm_mensagem").hide();
				}
				
			}
		});		
	}
	
	function fecharChat()
	{
		if(msgFechando==true)
		{
			return(true);
		}
		
		msgFechando=true;
		
		$.ajax({
			url: "usuario/chat/?acao=fechar",  
			success: function(retorno) {  
				document.location='<?=$v_base?>usuario/chat_finalizado';				
			}
		});
		
	}
	
	$(window).unload(function() {
		fecharChat();
	});
	
	$(document).ready(function(){
		
		$('#mensagem').bind('keypress', function(e) {
		    if(e.keyCode==13){
				enviarMsg();
				return(false);
		    }
		});
		
		window.setInterval(checarStatus,2000);
		atualizarMsg();
	
	});
	
	
	</script>
	
	<h1>Chat #<?=$_SESSION["chat"]?></h1>
	
	<div id="d_chat">
		
		<div id="d_atendente">
		Atendente: <?=$d_chat["nome_operador"]?><br />
		<span id="d_status"></span>
		</div>
		
		<div id="d_mensagens">
		<div>Iniciando...</div>
		</div>
		
		<form class="f_cadastro" id="frm_mensagem" action="usuario/chat" onsubmit="enviarMsg();return(false);">
		<input type="hidden" name="acao" value="postar"/>
		
			<span style="display: block;">
				
				<label>Mensagem</label>
				
				<textarea name="mensagem" id="mensagem" class="mm" style="width: 80%; height: 60px;"></textarea>
				
				<button type="submit">
				<img class="mm" src="imagens/botoes/reload.png"/>
				Enviar
				</button>
				
			</span>
			
			<span>
			<button type="button" onclick="fecharChat();">
			Fechar Chat
			</button>
			</span>
		
		</form>
	
	</div>
	<?
}
?>