<h1>Troca de Senha</h1>

<p>Utilize o formul&aacute;rio abaixo para trocar sua senha. S&oacute; que antes disso leia tamb&eacute;m algumas regras para manter sua senha segura.</p>

<p>
&nbsp;&nbsp; &bull; Tente utilizar caracteres mai&uacute;sculos e min&uacute;sculos na senha<BR>
&nbsp;&nbsp; &bull; Voc&ecirc; pode utilizar letras, n&uacute;meros ou qualquer caracter do teclado<BR>
&nbsp;&nbsp; &bull; A nova senha deve conter de seis (6) a oito (8) caracteres<BR>
&nbsp;&nbsp; &bull; A nova senha deve conter pelo menos tr&ecirc;s (3) letras e dois (2) n&uacute;meros
</p>

<form class="f_cadastro" method="post" action="trocasenha.php">

	<span>
	<label>Login</label>
	<input name="usuario" type="text" disabled class="myinputstyle" id="editbox" value="<?= $_GET['user']; ?>" size=20 maxlength=30>
	</span>
	
	<span>
	<label>Senha atual</label>
	<input name="atsenha" type=PASSWORD class="myinputstyle" id="editbox2" value="" size=20 maxlength=30>
	</span>
	
	<span>
	<label>Nova senha</label>
	<input name="newsenha1" type=PASSWORD class="myinputstyle" id="editbox3" value="" size=20 maxlength=20>
	</span>
	
	<span>
	<label>Confirma nova senha</label>
	<input name="newsenha2" type=PASSWORD class="myinputstyle" id="editbox4" value="" size=20 maxlength=20>
	</span>
	
	<span>
	<button type="submit">
	Trocar senha
	</button>
	</span>

</form>
