<div class="d_botoes">
	
    <?
    if(Central::permissao("dados"))
	{
		?>
		<a class="button" href="usuario/dados">
		<img src="imagens/64x64/mail_write.png"/>
		<span>Meus Dados</span>
		</a>
		<?
	}
    if(Central::permissao("extrato"))
	{
		?>
		<a class="button" href="usuario/extrato">
		<img src="imagens/64x64/calculator.png"/>
		<span>Extrato Financeiro</span>
		</a>
		<?
	}
    if(Central::permissao("acesso"))
	{
		?>
		<a class="button" href="usuario/acesso">
		<img src="imagens/64x64/calendar.png"/>
		<span>Relatório de Acessos</span>
		</a>
		<?
	}
    ?>

</div>

<div style="padding: 0 10px;">
	
	<?
	
	if(Central::permissao("extrato"))
	{
		include("index_extrato.php");
		?>
		<br />
		<?
	}
	
	
	if(Central::permissao("acesso"))
	{
		?>
		
		<h1>
			<a href="usuario/acesso_grafico">
			Acessos
			</a>
		</h1>
		
		<?
		
		$mikrotik=Conexao::conn("mikrotik");
		
		$r_login = mysql_query("SELECT DISTINCT username FROM radcheck WHERE id_cliente='{$_SESSION["usuario"]["id"]}' AND username NOT LIKE '%:%:%:%:%:%' ORDER BY username", $mikrotik);
				
		//$dt_inicio=($dt_inicio=="")?date("d/m/Y",strtotime("-30 day")):$dt_inicio;
		//$dt_final=($dt_final=="")?date("d/m/Y"):$dt_final;
		$dt_inicio=date("01/m/Y");
		$dt_final=date("t/m/Y");
		
		for($i=0;$i<mysql_num_rows($r_login);$i++)
		{
			$d_login=mysql_fetch_array($r_login);
			?>
			<div id="d_grafico_<?=$i?>" style="padding: 20px 0; text-align: center; width: 100%; height: 100%;">
			    <script type="text/javascript">
			    <?
			    $a_grafico=array();
			    $a_grafico["login"]=$d_login["username"];
			    $a_grafico["dt_inicio"]=$dt_inicio;
			    $a_grafico["dt_final"]=$dt_final;
			    $t_dados=base64_encode(json_encode($a_grafico));
			    ?>
                $(document).ready(function()
                {                
    			    var chart = new FusionCharts("framework/fusioncharts/FCF_MSArea2D.swf", "ChartId",800,"500");
    			    chart.setDataURL("usuario/acesso_grafico/?acao=dados%26dados="+escape("<?=$t_dados?>"));
    			    chart.setTransparent();		   
    			    chart.render("d_grafico_<?=$i?>");
                });
			    </script>
			</div>			
			<?
		}
		
	}
	?>
</div>