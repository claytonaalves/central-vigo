<?php
//require_once('Connections/vigo.php');

$Mostra_CodBarras = "S";
$Calcula_Juros = "S";

$Taxa_Juros = 2;
$Taxa_Mora  = 0.03;

$LstBancos = array(
   '001' => '001-9',
   '104' => '104-0',
   '356' => '356-5',
   '409' => '409-0',
   '341' => '341-7',
   '237' => '237-2',
   '099' => '099',
   '748' => '748-X',
   '399' => '399-9',
   '003' => '003-5',
   '004' => '004-3',
   '756' => '756-0',
   '041' => '041-8'
);


if ( !isset($_GET["NnR"] ) ) MensagemErro(); // Erro, parametro nosso numero nao informado

$Nnumero = mysql_real_escape_string($_GET["NnR"]);
//$IdCliente = $_GET["nId"];
$IdCliente = $_SESSION["usuario"]["id"];

// Pega os dados do boleto
//mysql_select_db($database_vigo, $vigo);
$query = mysql_query("SELECT * FROM financeiro_boletos WHERE nossonumero='$Nnumero' and numero='$IdCliente'") or die(mysql_error());
$boleto = mysql_fetch_assoc($query);

if ( mysql_num_rows($query) < 1 ) MensagemErro(); // Erro, nenhum boleto encontrado

mysql_free_result($query);

// Informacoes do banco
$IdBanco = $boleto['id_banco'];
$NumeroBanco = substr($boleto['linhadigitavel'], 0, 3);
$NumeroBancoI = $LstBancos[$NumeroBanco];
$IdEmpresa = $boleto['id_empresa'];
$Carteira = '';

// Informacoes do Boleto
$NossoNumero = $boleto['nossonumero'];
$CodigoBarras = $boleto['codigobarras'];
$LinhaDigitavel = $boleto['linhadigitavel'];
$Vencimento = date('d/m/Y', strtotime($boleto['vencimento']));
$DataProcessamento = date('d/m/Y', strtotime($boleto['emissao']));
$Data = date('d/m/Y');
$Instrucoes = $boleto['obs'];
$Valor = sprintf("%10.2f", $boleto['valor']);
$Juros = '';
$ValorCalculado = '';
$MesdeReferencia = MesExtenso(strftime('%m',strtotime($boleto['vencimento']))) . ' / ' . strftime('%Y',strtotime($boleto['vencimento']));

// Informacoes do Sacado
$NomeSacado = $boleto['nome'];
$Endereco = $boleto['endereco'];
$Cep = $boleto['cep'];
$Cidade = $boleto['cidade'];

// Pega as informacoes do Cedente
$query = mysql_query("SELECT rsocial, cnpj FROM sistema_empresas WHERE id=$IdEmpresa") or die(mysql_error());
$empresa = mysql_fetch_assoc($query);
$Cedente = $empresa['rsocial'] . " - CNPJ: " . $empresa['cnpj'];
mysql_free_result($query);

// Pega as informacoes do Banco
$query = mysql_query("SELECT agencia, conta FROM financeiro_bancos WHERE id='$IdBanco'") or die(mysql_error());
$banco = mysql_fetch_assoc($query);
$CONTA_DA_EMPRESA = $banco['agencia']."/".$banco['conta'];
mysql_free_result($query);

// Calculo de Juros
if ( $Calcula_Juros == 'S' ) {
   $Vencido = DiasEmAtraso($Vencimento);

   if ( $Vencido > 0 ) {
      $multa = $Valor * ($Taxa_Juros/100);
      $mora = ($Valor * ($Taxa_Mora/100)) * $Vencido;
      $Juros = $multa + $mora;
      
      $ValorCalculado = $Valor + $Juros;
      $Juros = sprintf("%10.2f", $Juros);
   }
   $ValorCalculado = sprintf("%10.2f", $ValorCalculado);
}

$qry = mysql_query("SELECT * FROM sistema_empresas ORDER BY id ASC limit 1", $vigo) or die(mysql_error());
$Empresa = mysql_fetch_assoc($qry);

if ( $Empresa['foto'] != "" )
   $logomarca = "empresa.jpg";
else
   $logomarca = "semlogo.jpg";

header("Content-type: text/html\n\n");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="pt-br"><head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"><title>Boleto</title>

<style type="text/css">
* { font-family: Verdana,Tahoma,Arial,Helvetica,sans-serif; font-size: 10px; }
body { margin: 10px 0 0 10px; background: #ffffff; }
table { border: 1px solid gray; border-collapse: collapse; padding: 0; width: 610px; position: relative;text-align: left;}

table.topo {border: 0px; width: 610px}
table.topo td {border: 0px; text-align: left; vertical-align: middle;}
table.topo img {border: 0px}

table.demonstrativo {width: 580px; margin-left: 10px; border: 0px}
table.demonstrativo td {border: 0px; height: 10px}

table.folha1 {width: 610px}
table.folha1 td {text-align: left; white-space: nowrap;}

table td { vertical-align: top; text-align: center; height: 25px; border: 1px solid; padding-top: 1px; }
table td#logo { border-right: 0; }
table td#Titulo { font-size: 18px; text-align: right; padding-top: 5px; padding-right: 15px; border-left: 0; }
table td#Num { padding-top: 8px; font-size: 14px; font-weight: bolder;}
table td#Numero { padding-top: 10px; font-size: 10px; font-weight: bolder;}

table td.Instru { padding: 10px 0 0 10px; text-align: left;}
table td.Rodape { font-weight: bolder; text-align: left; padding: 15px 0 0 15px; height: 65px; background:#e1e1e1; }
table td.esp { text-align: left; padding: 1px 0 0 5px; }
table td.Dir { text-align: left; padding: 1px 0 0 5px; width: 150px; }
table td.DirEsp { text-align: left; padding: 1px 0 0 5px; width: 150px; background:#eaeaea; }
table td.DirEsp2 { text-align: right; padding: 1px 5px 0 0; width: 150px; background:#eaeaea; }
table td.Texto1 { padding-left: 5px; text-align: left; }
div#Auten { width: 610px; position: static; text-align: right; padding: 0px 0 40px 0; border-bottom: 1px dotted gray; margin-bottom: 10px; }

.dado {font-weight: bold}
.mini {font-size: 8px}

</style>
</head><body>
<!-- ========================================================================= -->
<table class="topo">
<tbody>
<tr>
<td><a href="<?=$Empresa['site']?>"><img src="<?=$logomarca?>"></a></td>
<td style="width: 109px;"></td>
<td><?=$Empresa['fantasia']?><br>
<?=$Empresa['endereco']?>, <?=$Empresa['bairro']?><br>
Cidade <?=$Empresa['cidade'].' - '.$Empresa['uf']?>, CEP <?=$Empresa['cep']?><br>
CNPJ <?=$Empresa['cnpj']?></td>
</tr>
</tbody>
</table>
<br>

<!-- ========================================================================= -->

<table class="folha1">
<tbody>
<tr>
   <td colspan="2">Cedente<br><span class="dado">&nbsp;&nbsp;<?= $Empresa['rsocial']?></span></td>
   <td >Ag�ncia/Cod. Cedente<br><span class="dado">&nbsp;&nbsp;<?= $CONTA_DA_EMPRESA?></span></td>
   <td>Data de Emiss�o<br><span class="dado">&nbsp;&nbsp;<?= $DataProcessamento?></span></td>
   <td>Vencimento<br><span style="font-weight: bold;">&nbsp;&nbsp;<?= $Vencimento?></span></td>
</tr>
<tr>
   <td colspan="2">Sacado<br><span class="dado">&nbsp;&nbsp;<?= $NomeSacado?></span></td>
   <td colspan="2">N�mero Documento<br>&nbsp;<span class="dado"><?= ""?></span></td>
   <td>Nosso Numero<br>&nbsp;<span class="dado"><?= $NossoNumero?></span></td>
</tr>
<tr>
   <td>Esp�cie<br>&nbsp;<span class="dado"><?= "R$"?></span></td>
   <td>Quantidade Moeda<br> &nbsp;<span class="dado"><?= ""?></span></td>
   <td>Valor<br>&nbsp;<span class="dado"><?= ""?></span></td>
   <td>Valor Documento<br> &nbsp;<span class="dado"><?= $Valor?></span></td>
   <td>Descontos/Abatimentos<br> &nbsp;<span class="dado"><?= ""?></span></td>
</tr>
<tr>
<td colspan="5">
Demonstrativo<br>
<br>

<!-- ========================================================================= -->

<table class="demonstrativo">
   <tbody>
      <tr>
         <td class="dado">Hist�ricos:</td>
         <td class="dado">Quantidade</td>
         <td class="dado">Valor Unit�rio</td>
         <td class="dado">Valor Total</td>
      </tr>
      <?php
      $qry = mysql_query("SELECT descricao, valor FROM financeiro_planos_clientes WHERE idcliente='$IdCliente'") or die(mysql_error());
      $Planos = mysql_fetch_assoc($qry);
      $TotalServicos = 0;
      
      do {
         $TotalServicos += $Planos['valor'];
      ?>
      <tr>
         <td>- <?=$Planos['descricao']?></td>   
         <td>1,00&nbsp;&nbsp;&nbsp;&nbsp;x</td>
         <td><?= sprintf("%10.2f", $Planos['valor'])?>&nbsp;&nbsp;&nbsp;&nbsp;=</td>
         <td><?= sprintf("%10.2f", $Planos['valor'])?></td>
      </tr>
      <?php
      } while ($Planos = mysql_fetch_assoc($qry));
      ?>
      <tr>
         <td></td>
         <td></td>
         <td></td>
         <td style="border-top: 1px solid" class="dado"><?=$TotalServicos?></td>
      </tr>
   </tbody>
</table>

<!-- ========================================================================= -->

(*) - IMPOSTOS INCLUSOS - ICMS, PIS, COFINS, FUST E FUNTEL
<br>
<br>
</td>
</tr>
<tr>
<td colspan="5">Observa��es<br>
&nbsp; &nbsp;<span class="dado"><?=$MesdeReferencia?></span>
<br>
<br>
</td>
</tr>
</tbody>
</table>

<!-- ========================================================================= -->

<div id="Auten"><span class="mini">Autentica��o
Mec�nica / Recibo do Sacado</span></div>

<!-- ========================================================================= -->

<table>
<tbody>
<tr>
   <td><img src="banco-<?=$NumeroBanco?>.bmp"></td>
   <td id="Num"><?=$NumeroBancoI?></td>
   <td colspan="4" id="Numero"><?=$LinhaDigitavel?></td>
</tr>
<tr>
   <td colspan="5" class="Texto1">Local de pagamento<br>&nbsp;&nbsp;<b>QUALQUER AG�NCIA BANC�RIA AT� O VENCIMENTO</b></td>
   <td class="DirEsp2">Vencimento<br><span class="dado"><?=$Vencimento?></span></td>
</tr>
<tr>
   <td colspan="5" class="Texto1">Cedente<br>&nbsp;&nbsp;<b><?=$Cedente?></b></td>
   <td class="Dir">Ag�ncia / C�digo<br>&nbsp;&nbsp;<?=$CONTA_DA_EMPRESA?></td>
</tr>
<tr>
   <td>Data<br><b><?=$Data?></b></td>
   <td>Documento</td>
   <td>Esp�cie<br><b>DS</b></td>
   <td>Aceite<br><b>N</b></td>
   <td>Dt.Processamento<br><b><?=$DataProcessamento?></b></td>
   <td class="Dir">Nosso N�mero<br>&nbsp;&nbsp;<?=$NossoNumero?></td>
</tr>
<tr>
   <td>Conta</td>
   <td>Carteira<br><b><?=$Carteira?></b></td>
   <td>Esp�cie<br><b>Real</b></td>
   <td>Quantidade<br></td>
   <td>Valor</td>
   <td class="DirEsp">(=) Valor do documento<br>&nbsp;&nbsp;<b><?=$Valor?></b></td>
</tr>
<tr>
   <td rowspan="5" colspan="5" class="Instru"><b>INSTRU��ES:</b><br>*** VALORES EM REAIS ***<br><br><pre><?=$Instrucoes?></pre></td>
   <td class="Dir">(-) Desconto<br>&nbsp;&nbsp;0.00</td>
</tr>
<tr>
   <td class="Dir">(-) Outras dedu��es<br>&nbsp;&nbsp;0.00</td>
</tr>
<tr>
   <td class="Dir">(+) Mora / Multa / Juros<br>&nbsp;&nbsp;<b><?=$Juros?></b></td>
</tr>
<tr>
   <td class="Dir">(+) Outros acr�scimos<br></td>
</tr>
<tr>
   <td class="Dir">(=) Valor cobrado<br>&nbsp;&nbsp;<b><?=$ValorCalculado?></b></td>
</tr>
<tr>
   <td colspan="6" class="Rodape"><?=$NomeSacado?><br><?=$Endereco?><br><?=$Cep?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$Cidade?></td>
</tr>
</tbody>
</table>

<div style="width: 610px; text-align: left; padding-top: 5px;">
<?php
if ( $Mostra_CodBarras == 'S' )
   print "<img src='codbarra.php?numero=$CodigoBarras'>";
?>
</div>
</body></html>

<?php

function DiasEmAtraso($data) {
   $ano = substr($data,6,4);
   $mes = substr($data,3,2);
   $dia = substr($data,0,2);
   
   $data_atual = date("d/m/Y");
   $ano_atual = substr($data_atual,6,4);
   $mes_atual = substr($data_atual,3,2);
   $dia_atual = substr($data_atual,0,2);

   $data = mktime(0, 0, 0, $mes, $dia, $ano);
   $data_atual = mktime(0, 0, 0, $mes_atual, $dia_atual, $ano_atual);
   
   $dias = ($data_atual - $data)/86400;
   $dias = ceil($dias);
   
   if ( $dias < 0 )
      $dias = 0;   
   
   return $dias;
}

function MesExtenso($nmes) {
   switch ($nmes) {
      case "01": $mes = 'Janeiro';     break;
      case "02": $mes = 'Fevereiro';   break;
      case "03": $mes = 'Mar�o';       break;
      case "04": $mes = 'Abril';       break;
      case "05": $mes = 'Maio';        break;
      case "06": $mes = 'Junho';       break;
      case "07": $mes = 'Julho';       break;
      case "08": $mes = 'Agosto';      break;
      case "09": $mes = 'Setembro';    break;
      case "10": $mes = 'Outubro';     break;
      case "11": $mes = 'Novembro';    break;
      case "12": $mes = 'Dezembro';    break; 
   }
   return $mes;
}

function MensagemErro() {
   print "<html><title>VIGOprovider</title><font face=Verdana size=+1>ERRO !!!<br>";
   print "N&atilde;o existem boletos em aberto ou<br>";
   print "N&atilde;o foi selecionado nenhum boleto da lista anterior<br><br>";
   print "Para maiores informa&ccedil;&otilde;es contate o WEBMASTER.<br>";
   print "<br><a href='javascript:history.go(-1);'>Clique aqui para retornar</a></font></html>";
   exit;
}

?>