<?php
// TODO: pegar informacoes da empresa relativa ao cliente

include_once('phpmailer/class.phpmailer.php');
//require_once('Connections/vigo.php');
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-2">
<title>Recuperar senha de acesso a Central do Assinante</title>
<link href="formulario.css" rel="stylesheet" type="text/css">
<link href="css.css" rel="stylesheet" type="text/css">
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {font-family: "Trebuchet MS"; font-size: 14px; color: #FF0000; }
.style3 {font-family: "Trebuchet MS"; font-size: 14px; color: #99CC00; }
-->
</style>
</head>

<body>
<div align="center" class="titulo2"><p>RECUPERAR SENHA DE ACESSO</p></div>
<br>

<?php
if (isset($_POST['login'])) {
   if($_POST['login'] != ""){
?>

<table width="500" border="0" align="center" cellpadding="5" cellspacing="0" class="myinputstyle">
  <tr>
    <td><div align="center"><span class="style1">
      <img src="images/sinfo_16.png" width="16" height="16"><br>
<?php
if( $_GET['acao'] == "enviar" ) {

   //mysql_select_db($database_vigo, $vigo);

   $login = $_POST['login'];
   
   $usuario = mysql_query("SELECT nome, email, login, senha, idempresa FROM cadastro_clientes WHERE login='$login'", $vigo) or die(mysql_error());
   $row_usuario = mysql_fetch_assoc($usuario);
   $totalRows_usuario = mysql_num_rows($usuario);
   
   $idempresa = $row_usuario['idempresa'];

   $empresa = mysql_query("SELECT * FROM sistema_empresas WHERE id=$idempresa", $vigo) or die(mysql_error());
   $row_empresa = mysql_fetch_assoc($empresa);
   $totalRows_empresa = mysql_num_rows($empresa);

   if ( $totalRows_usuario == "0" )
      print "O Login informado n&atilde;o consta em nossa base de dados, qualquer d&uacute;vida entre em contato com o seu provedor no telefone: <strong>". $row_empresa['telefone'] ."</strong></span>";
   else { 
      if ( $row_usuario['email'] == "" )
         print '<span class="style1">Nao existe e-mail cadastrado em sua conta. Entre em contato com seu provedor no telefone: <strong>' . $row_empresa['telefone'] . '</strong></span>';
      else {
         print '<span class="style3">Sua senha foi enviada para o e-mail: <strong><i>' . $row_usuario['email'] . '</i></strong></span>';
         
         $login = $row_usuario['login'];
         $senha = $row_usuario['senha'];

         $fantasia = $row_empresa['fantasia'];
         $endereco = $row_empresa['endereco'];
         $telefone1 = $row_empresa['telefone'];
         $cidade = $row_empresa['cidade'];
         $cep = $row_empresa['cep'];
         $email_empresa = $row_empresa['email'];
         $site = $row_empresa['site']; 

         $assunto = "Senha de acesso recuperada";
         $mensagem1 = "============ Senha Recuperada ============<br><br>";
         $mensagem1.= "Usuario: <b>$login</b><br>";
         $mensagem1.= "Senha: <b>$senha</b><br><br>";
         $mensagem1.= "$fantasia<br>Telefone: $telefone1<br>End: $endereco<br>$cep - $cidade<br>Site: $site<br><br>";
         $mensagem1.= "==========================================";
         
         // -------------------------------------------------------------------------
         $mail = new PHPMailer();
         $mail->IsSMTP();
         $mail->Host = 'localhost';
         $mail->From = $email_empresa;
         $mail->FromName = $fantasia;
         
         $mail->AddReplyTo($email_empresa, $fantasia);
         $mail->AddAddress("clayton@vigo.com.br", 'Clayton A. Alves');
         //~ $mail->AddAddress($row_usuario['email'], $row_usuario['nome']);
         
         $mail->Subject = $assunto;
         $mail->Body = $mensagem1;
         $mail->IsHTML(true); 

         if(!$mail->Send()) {
            echo "Erro ao enviar: " . $mail->ErrorInfo;
         } else {
            echo "Mensagem enviada com sucesso!";
         }
      }
   }
}
?>
    </div></td>
  </tr>
</table>

<?php }
} ?>

<form name="form1" method="post" action="?acao=enviar">
  <table width="500" border="0" align="center" cellpadding="2" cellspacing="2" class="myinputstyle">
      <tr><td><br></td></tr>
    <tr>
      <td width="145"><div align="right" class="texto_negrito">Digite seu login:</div></td>
      <td width="355"><span id="sprytextfield1">
         <input name="login" type="text" class="myinputstyle" id="login">
         <span class="textfieldRequiredMsg"> <img src="images/sinfo_16.png"> Campo requerido.</span></span></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>
        <input name="button" type="submit" class="myinputstyle" id="button" value="Recuperar senha">
      </td>
    </tr>
    <tr><td><br></td></tr>
  </table>
</form>

<div align="center">
   <hr size="1">
   <div align="center">
      <span class="mylabelstyle">
         A senha do assinante &eacute; secreta, n&atilde;o a informe a ningu&eacute;m.<BR>
         Nenhum funcion&aacute;rio   do Provedor est&aacute; autorizado a solicit&aacute;-la.<BR>
         Os dados cadastrais s&atilde;o confidenciais.
      </span>
   </div>
   <hr size="1">
</div>

<script type="text/javascript">
   <!--
   var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "none", {validateOn:["change"]});
   //-->
</script>

</body>
</html>

<?php
if (isset($usuario)) mysql_free_result($usuario);
if (isset($empresa)) mysql_free_result($empresa);
?>