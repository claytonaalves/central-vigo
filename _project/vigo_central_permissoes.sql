/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

/*Table structure for table `central` */

DROP TABLE IF EXISTS `central`;

CREATE TABLE `central` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cor` varchar(20) DEFAULT NULL,
  `email_suporte` varchar(100) DEFAULT NULL,
  `text_contrato` text,
  `permissoes` varchar(500) DEFAULT NULL,
  `multa` double default 2,
  `juros` double default 0.33,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

/*Data for the table `central` */

insert  into `central`(`id`,`cor`,`email_suporte`,`text_contrato`,`permissoes`) 
values (null,'azul','indefinido@indefinido.com.br','',
        'acesso,acesso_grafico,mksenha,dados_atualiza,dados,email,foto,senha,2via,contrato,contrato_obrigatorio,extrato,nfe,servicos,chat,chats,download,recados,suporte,suporte_cad,suporte_contato');

/*Table structure for table `central_permissoes` */

DROP TABLE IF EXISTS `central_permissoes`;

CREATE TABLE `central_permissoes` (
  `id` varchar(50) NOT NULL,
  `grupo` varchar(50) DEFAULT NULL,
  `nome` varchar(100) DEFAULT NULL,
  `exibir` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `central_permissoes` */

insert  into `central_permissoes`(`id`,`grupo`,`nome`,`exibir`) values ('dados','dados','Dados',1);
insert  into `central_permissoes`(`id`,`grupo`,`nome`,`exibir`) values ('dados_atualiza','dados','Atualizar Dados',1);
insert  into `central_permissoes`(`id`,`grupo`,`nome`,`exibir`) values ('senha','dados','Senha',1);
insert  into `central_permissoes`(`id`,`grupo`,`nome`,`exibir`) values ('foto','dados','Foto',1);
insert  into `central_permissoes`(`id`,`grupo`,`nome`,`exibir`) values ('email','dados','E-mail',0);
insert  into `central_permissoes`(`id`,`grupo`,`nome`,`exibir`) values ('extrato','extrato','Extrato',1);
insert  into `central_permissoes`(`id`,`grupo`,`nome`,`exibir`) values ('2via','extrato','2ª Via',1);
insert  into `central_permissoes`(`id`,`grupo`,`nome`,`exibir`) values ('servicos','extrato','Serviços',1);
insert  into `central_permissoes`(`id`,`grupo`,`nome`,`exibir`) values ('contrato','extrato','Contrato',1);
insert  into `central_permissoes`(`id`,`grupo`,`nome`,`exibir`) values ('contrato_obrigatorio','extrato','Contrato - Confirmar',0);
insert  into `central_permissoes`(`id`,`grupo`,`nome`,`exibir`) values ('nfe','extrato','Nota Fiscal',1);
insert  into `central_permissoes`(`id`,`grupo`,`nome`,`exibir`) values ('mksenha','acesso','Senha Acesso',1);
insert  into `central_permissoes`(`id`,`grupo`,`nome`,`exibir`) values ('acesso','acesso','Acesso',1);
insert  into `central_permissoes`(`id`,`grupo`,`nome`,`exibir`) values ('acesso_grafico','acesso','Acesso - Gráfico',1);
insert  into `central_permissoes`(`id`,`grupo`,`nome`,`exibir`) values ('recados','suporte','Recados',1);
insert  into `central_permissoes`(`id`,`grupo`,`nome`,`exibir`) values ('download','suporte','Downloads',1);
insert  into `central_permissoes`(`id`,`grupo`,`nome`,`exibir`) values ('suporte','suporte','Suporte',1);
insert  into `central_permissoes`(`id`,`grupo`,`nome`,`exibir`) values ('suporte_cad','suporte','Suporte - Solicitar',1);
insert  into `central_permissoes`(`id`,`grupo`,`nome`,`exibir`) values ('suporte_contato','suporte','Suporte - Via E-mail',1);
insert  into `central_permissoes`(`id`,`grupo`,`nome`,`exibir`) values ('chat','suporte','Chat',1);
insert  into `central_permissoes`(`id`,`grupo`,`nome`,`exibir`) values ('chats','suporte','Chat - Histórico',1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
