<?php
ob_clean();

$r_empresas = mysql_query("SELECT * FROM empresas ORDER BY id ASC",$vigo);

if($acao=="login") 
{
	$res=Operadores::login($_POST['login'],$_POST['senha'],$POST['empresa']);
	if($res)
	{
		exit(header("Location: {$v_base}operador/index"));
	}
	else
	{
		$infomensagem="Login e/ou senha n�o encontrados!";
	}
}

Operadores::logoff();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=windows-1252"/>
	<title><?php echo $v_title?></title>
	
    <?php
    if($v_base!="")
    {
        ?>
        <base href="<?=$v_base?>"/>
        <?php    
    }
    ?>    
    
	<link rel="stylesheet" type="text/css" href="framework/framework.css"/>
	<link rel="stylesheet" type="text/css" href="css.css"/>
	<?php
	if($box!="")
	{
		?>
		<link rel="stylesheet" type="text/css" href="css_box.css"/>
		<?php
	}
	?>
	
	<link rel="shortcut icon" href="favicon.ico"/>
	
	
	<script type="text/javascript" src="framework/jquery-1.5.1.min.js"></script>
	
	<link type="text/css" href="framework/smoothness/jquery-ui-1.8.13.custom.css" rel="stylesheet" />	
	<script type="text/javascript" src="framework/jquery-ui-1.8.13.custom.min.js"></script>
	
	<script type="text/javascript" src="framework/jquery.ui.datepicker-pt-BR.js"></script>
	<script type="text/javascript" src="framework/jquery.ui.timepicker.js"></script>
	<script type="text/javascript" src="framework/jquery.maskedinput-1.2.2.js"></script>
	<script type="text/javascript" src="framework/jquery.maskMoney.0.2.js"></script>
	<script type="text/javascript" src="framework/jquery.alphanumeric.js"></script>
	
	<script type="text/javascript" language="javascript" src="framework/fancybox/jquery.fancybox-1.2.6.js"></script>
	<link rel="stylesheet" href="framework/fancybox/jquery.fancybox-1.2.6.css" type="text/css" media="screen"/>
	
	<script type="text/javascript" src="framework/fusioncharts/fusioncharts.js"></script>
	
	<script type="text/javascript" src="framework/ajaxutil.js"></script>
	<script type="text/javascript" src="javascript.js"></script>
	
	<script type="text/javascript" src="framework/framework_mask.js"></script>	
	
	<style type="text/css">
	html, body, table, form {
		height: 100%;
	}
	table td {
		padding: 60px 20px;
	}
	.d_login_empresas div {
		white-space: nowrap;
		height: 80px;
		margin: 10px 0;
		overflow: hidden;
	}
	</style>
	
</head>

<body>

<a class="off off_reverso" href="usuario/index" style="position: absolute; bottom: 0; right: 0; margin: 5px;">
usu�rio <img class="mm" src="design/ico_troca.png"/></a>

<form action="operador/login" method="post">
<input type="hidden" name="acao" value="login"/>

<table style="width: 100%; height: 100%;">
<tr valign="top">

	<td style="width: 45%; background: #FFF;">
		
		<!--
		<h1>
		<img class="mm" src="design/ico_operador.gif"/>
		Central do Operador</h1>
		
		<p>A Central do Operador &eacute; uma &aacute;rea destinada ao operador.</p>
		<p>Aqui voc&ecirc; pode:</p>
		
		<ul>
		<li>Alterar seus dados</li>
		<li>Acessar a Ajuda</li>
		<li>Ver informa&ccedil;&otilde;es sobre sua conta</li>
		<li>2&ordf; Via de Boleto</li>
		<li>Extrato financeiro</li>
		</ul>
		-->
		
		<div class="d_login_empresas">
		<?
		for($i=0;$i<mysql_num_rows($r_empresas);$i++)
		{
			$d_empresas=mysql_fetch_array($r_empresas);
			$t_checked=( ($i==0 AND $_POST["empresa"]=="") OR $_POST["empresa"]==$d_empresas["id"])?' checked=""':"";
			?>
			<div>
				
				<!--
				<input class="mm" type="radio" name="empresa" value="<?=$d_empresas["id"]?>"<?=$t_checked?>/>
				-->
				
				<a href="<?=$d_empresas["site"]?>" target="_blank">
				<img class="mm" src="imagizer_export.php?<?=Empresas::logomarca($d_empresas["id"])?>,140,80,1,0,,jpg" style="float: left; margin: 0 5px 0 0;"/></a>
				
				<br />
				
			    <strong>
			    <!--
				<?=$d_empresas["rsocial"]?><br />
				-->
			    <?=$d_empresas["fantasia"]?><br />
			    </strong>
			    <small>
			    <?=$d_empresas["endereco"]?><br />
				<?=$d_empresas["bairro"]?> / <?=$d_empresas["cidade"]?> / <?=$d_empresas["uf"]?> / <?=$d_empresas["cep"]?><br />
			    <?=$d_empresas["telefone"]?> / <?=$d_empresas["email"]?>
			    </small>
			    
			</div>
			<?
		}
		?>
		</div>		
        
	</td>
	
	<td style="width: 55%;">
		
		<h1>
		<img class="mm" src="design/ico_operador.gif"/>
		Central do Operador</h1>		
		
		<div class="f_cadastro">
			
			<span>
			<label>Login</label>
			<input type="text" name="login" size="15"/>
			</span>
			
			<span>
			<label>Senha</label>
			<input type="password" name="senha" size="15"/>
			</span>
			
			<span>
				<button type="submit">
				Acessar</button>
			</span>
			
		</div>
            
	</td>

</tr>
</table>

</form>

<script type="text/javascript" defer="defer">
<?
if($infomensagem!="")
{
	$infomensagem=str_replace("<br>","\\n",$infomensagem);
	?>
	alert('<?=$infomensagem?>');
	<?php
}
?>
</script>

</body>
</html>
<?
exit();
?>