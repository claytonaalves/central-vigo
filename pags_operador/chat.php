<?
header("Content-Type: text/html; charset=iso-8859-1");

if($acao=="atender")
{
	Chat::atender($chat);
}

$d_chat=Chat::dados($chat);

if($acao=="postar")
{
	ob_clean();
	
	$a_dados=array();
	$a_dados["mensagem"]=utf8_decode($mensagem);
	$a_dados["usuario"]="";
	$a_dados["operador"]=$_SESSION["operador"]["login"];
	
	Chat::postar($chat,$a_dados);
	
	exit();
}

if($acao=="comentar")
{
	ob_clean();
	
	Chat::comentar($chat,utf8_decode($comentario));
	
	exit();
}

if($acao=="mensagens")
{
	ob_clean();
	
	Chat::mensagens($chat);
	
	exit();
}

if($acao=="status")
{
	ob_clean();
	
	// Ping operador
	
	Chat::ping($chat,"operador");
	
	// Busca id ultima mensagem
	
	$ultimaMsg=Chat::mensagensIdUltima($chat);
	
	?>
	msgUltima='<?=$ultimaMsg?>';
	<?	
	
	// Verifica status do chat
	
	$d_chat=Chat::dados($chat);
	
	?>
	msgStatus='<?=$d_chat["status"]?>';
	<?
	
	if($d_chat["status"]==2)
	{
		?>
		$("#d_status").html('Status: OK');
		<?
	}
	
	if($d_chat["status"]==1)
	{
		?>
		$("#d_status").html('Status: Finalizado');
		<?
	}
	
	// Checa ausencia
	
	$t_ausencia=Chat::checarAusencia($_SESSION["chat"]);
	
	if($t_ausencia=="usuario")
	{
		?>
		$("#d_status").html('O usu�rio n�o est� mais no chat.');
		<?
		exit();
	}
	
	exit();
}

if($acao=="fechar")
{
	ob_clean();
	
	$a_dados=array();
	$a_dados["mensagem"]="* Operador fechou chat!";
	$a_dados["usuario"]="";
	$a_dados["operador"]=$_SESSION["operador"]["login"];
	$a_dados["info"]="1";
	
	Chat::postar($chat,$a_dados);	
	
	Chat::fechar($chat);
	
	exit();
}

?>
<link rel="stylesheet" type="text/css" href="css_chat.css"/>

<script type="text/javascript">

function enviarMsg()
{
	msg=$("#mensagem").val();
	$("#mensagem").val("");
	
	if(msg=="")
	{
		return(false);
	}
	
	$.ajax({  
		type: "POST",  
		url: "operador/chat/?chat=<?=$chat?>&acao=postar",  
		data: "mensagem="+msg,
		async: false,
		success: function(retorno) {  
			  atualizarMsg();
			  msgRolagem(true);
		}  
	});
	
	return(false);
}

msgErros=0;
msgStatus=0;
msgAtual="";
msgUltima="";

function atualizarMsg()
{
	$("#d_status").html("Checando...");

	if(msgErros>30)
	{
		alert("Conex�o caiu!");
	}

	$.ajax({  
 		url: "operador/chat/?chat=<?=$chat?>&acao=mensagens",  
		success: function(retorno) {  
			$("#d_mensagens div").html(retorno);
			$("#d_status").html("Atualizado.");
			msgErros=0;
			msgRolagem();
			msgAtual=msgUltima;
			
			$(window).blur(function(){
			    //$(window).focus();
			});			
			
		},
		error: function() {
			msgErros++;
		}
	});
	
}

function msgRolagem(paraFinal)
{
	var psconsole = $('#d_mensagens');
	
	t_scroll=psconsole[0].scrollHeight - psconsole.height();
	
	if( psconsole.scrollTop()>(t_scroll-50) || paraFinal==true )
	{
		psconsole.scrollTop(t_scroll);
	}
	
}

function checarStatus()
{
	$.ajax({  
 		url: "operador/chat/?chat=<?=$chat?>&acao=status",  
		success: function(retorno) {  
			eval(retorno);
			
			if(msgAtual!=msgUltima)
			{
				atualizarMsg();
			}
			
			if(msgStatus=='1')
			{
				$("#frm_mensagem").hide();
			}
			
		}
	});		
}

function comentarioAtualizar(obj)
{
	texto=obj.value;
	
	$.ajax({  
		type: "POST",  
		url: "operador/chat/?chat=<?=$chat?>&acao=comentar",  
		data: "comentario="+texto,
		success: function(retorno) {  
			$("#d_status").html("Coment�rio atualizado.");
		}
	});	
	
}

function fecharChat()
{
	if(confirm("Deseja fechar o chat?"))
	{
		$.ajax({
			url: "operador/chat/?chat=<?=$chat?>&acao=fechar"
		});
	}
}

$(window).unload(function() {
	//fecharChat();
});

$(document).ready(function(){
	
	$('#mensagem').bind('keypress', function(e) {
	    if(e.keyCode==13){
			enviarMsg();
			return(false);
	    }
	});
	
	<?
	if($d_chat["status"]!=1)
	{
		?>	
		window.setInterval(checarStatus,2000);
		<?
	}
	?>	
	atualizarMsg();
	
	tabs();

});

function tabs()
{
	$("#d_tabs div").hide();
	$("#d_tabs div:eq(0)").show();
	
	$('#d_tabs_menu a').click(function() {
		tab=$(this).attr("tab");

		$("#d_tabs div").hide();
		$("#d_tabs div:eq("+tab+")").show();
	});
	
	ajaxHTML("aba_atendimento","operador/chat_aba_atendimento/?id=<?=$chat?>")
	ajaxHTML("aba_dados","operador/chat_aba_dados/?id=<?=$d_chat["usuario"]?>")
	ajaxHTML("aba_extrato","operador/chat_aba_extrato/?id=<?=$d_chat["usuario"]?>")
	ajaxHTML("aba_chats","operador/chat_aba_chats/?id=<?=$d_chat["usuario"]?>")

}

</script>

<h1>Chat #<?=$chat?></h1>

<table style="width: 100%;" cellpadding="5" cellspacing="0">
<tr valign="top">

	<td style="width: 50%;">
	
		<div id="d_chat">
			
			<div id="d_atendente">
			Atendente: <?=$d_chat["nome_operador"]?><br />
			<span id="d_status"></span>
			</div>
			
			<div id="d_mensagens">
			<div>Iniciando...</div>
			</div>
			
			<?
			if($d_chat["status"]!=1)
			{
				?>
				<form class="f_cadastro" id="frm_mensagem" action="operador/chat" onsubmit="enviarMsg();return(false);">
				<input type="hidden" name="acao" value="postar"/>
				
					<span style="display: block;">
						
						<label>Mensagem</label>
						
						<textarea name="mensagem" id="mensagem" class="mm" style="width: 80%; height: 60px;"></textarea>
						
						<button type="submit">
						<img class="mm" src="imagens/botoes/reload.png"/>
						Enviar
						</button>
						
					</span>
					
					<span>
					<button type="button" onclick="fecharChat();">
					Fechar Chat
					</button>
					</span>
				
				</form>
				<?
			}
			?>
		
		</div>
		
	</td>
	
	<td style="width: 50%;">
		
		<div id="d_tabs_menu" class="d_menu">
			<a tab="0">Atendimento</a>
			<a tab="1">Dados Cliente</a>
			<a tab="2">Hist�rico Chat</a>
			<a tab="3">Extrato Financeiro</a>
		</div>

		<div id="d_tabs">

			<div id="aba_atendimento">
			</div>
			
			<div id="aba_dados">
			</div>
			
			<div id="aba_chats">
			</div>
			
			<div id="aba_extrato">
			</div>
		
		</div>
	
	</td>
	
</tr>
</table>