<?
class Chat {

	public static function getStatus($status)
	{
		$a_status=array( 0=>"Aguardando", 1=>"Fechado", 2=>"Em Atendimento", 3=>"Transfer�ncia" );
		return($a_status[$status]);
	}
	
	public static function iniciar($a_dados=array(),$id="")
	{
		$v_tabela="chat";
    	$a_campos=Suporte::mysql_field_array($v_tabela);
    	array_shift($a_campos);
		
		$data=date("Y-m-d H:i:s");
		$usuario=$a_dados["usuario"];
		$empresa=$a_dados["empresa"];
		$setor=$a_dados["setor"];
		
		$nome=$a_dados["nome"];
		$email=$a_dados["email"];
		$telefone=$a_dados["telefone"];
		
		$ping_usuario=$data;
		$ping_operador=$data;
		
		$operador="";
		$status="0";
		
    	for($c=0;$c<count($a_campos);$c++) { $a_campos2[$c]=mysql_real_escape_string(${$a_campos[$c]}); }
    	//for($c=1;$c<count($a_campos);$c++) { $a_campos3[$c]=$a_campos[$c]."='".mysql_real_escape_string(${$a_campos[$c]})."'"; }
    	$sql1=implode(",",$a_campos);
    	$sql2=implode("','",$a_campos2);
    	//$sql3=implode(",",$a_campos3);
		
		$resultado=mysql_query("INSERT INTO {$v_tabela} ({$sql1}) VALUES ('{$sql2}')");
		$id=mysql_insert_id();
		
		if($a_dados["mensagem"]!="")
		{
			Chat::postar($id,$a_dados);
		}
		
		return($id);
						
	}
	
	public static function postar($chat,$a_dados=array())
	{
    	
		$v_tabela="chat_mensagem";
    	$a_campos=Suporte::mysql_field_array($v_tabela);
		
		$data=date("Y-m-d H:i:s");
		$chat=intval($chat);
		$usuario=$a_dados["usuario"];
		$operador=$a_dados["operador"];
		$info=intval($a_dados["info"]);
		$status="0";
		
		$mensagem=Suporte::protege($a_dados["mensagem"]);
		//$mensagem=Suporte::protege($a_dados["mensagem"]);
		
    	for($c=0;$c<count($a_campos);$c++) { $a_campos2[$c]=${$a_campos[$c]}; }
    	for($c=1;$c<count($a_campos);$c++) { $a_campos3[$c]=$a_campos[$c]."='".${$a_campos[$c]}."'"; }
    	$sql1=implode(",",$a_campos);
    	$sql2=implode("','",$a_campos2);
    	$sql3=implode(",",$a_campos3);
		
		$resultado=mysql_query("INSERT INTO {$v_tabela} ({$sql1}) VALUES ('{$sql2}')");
		   		
	}
	
	public static function mensagens($chat)
	{
		$sqlcampos="";
		//$sqlcampos.=", (SELECT nome FROM usuarios WHERE usuarios.numero=chat_mensagem.usuario) AS nome_usuario";
		//$sqlcampos.=", (SELECT nome FROM usuarios WHERE usuarios.numero=chat_mensagem.usuario) AS nome_usuario";
		$sqlcampos.=", (SELECT nome FROM operadores WHERE operadores.login=chat_mensagem.operador) AS nome_operador";
	
		$r_chat_mensagem = mysql_query("SELECT *{$sqlcampos} FROM chat_mensagem WHERE chat='{$chat}' ORDER BY id ASC");
		
		$r_chat = mysql_query("SELECT * FROM chat WHERE id='{$chat}'");
		$d_chat = mysql_fetch_array($r_chat);
		
		//$a_dados=array();
		for($i=0;$i<mysql_num_rows($r_chat_mensagem);$i++)
		{
			$a_mensagem=mysql_fetch_array($r_chat_mensagem);
			
			$t_class=($a_mensagem["info"]=="1")?" msg_info":"";
			
			if($a_mensagem["operador"]!="")
			{
				?>
				<span class="msg_operador<?=$t_class?>">
					<small><?=date("d/m h:i",strtotime($a_mensagem["data"]))?></small>
					<b><?=$a_mensagem["nome_operador"]?></b>
					<p><?=$a_mensagem["mensagem"]?></p>
				</span>
				<?
			}
			else
			{
				//mensagem do usu�rio
				?>
				<span class="msg_usuario<?=$t_class?>">
					<small><?=date("d/m h:i",strtotime($a_mensagem["data"]))?></small>
					<b><?=$d_chat["nome"]?></b>
					<p><?=$a_mensagem["mensagem"]?></p>
				</span>
				<?			
			}

		}
		
		//return($a_dados);
		
	}
	
	public static function mensagensIdUltima($chat)
	{
		$r_chat_mensagem = mysql_query("SELECT id FROM chat_mensagem WHERE chat='{$chat}' ORDER BY id DESC LIMIT 1");
		
		if(mysql_num_rows($r_chat_mensagem)==1)
		{
			return(mysql_result($r_chat_mensagem,0,0));
		}
		else
		{
			return("0");
		}
				
	}
	
	public static function ping($chat,$para)
	{
		$agora=date("Y-m-d H:i:s");
		
		if($para=="operador")
		{
			mysql_query("UPDATE chat SET ping_operador='{$agora}' WHERE id='{$chat}'");
		}
		if($para=="usuario")
		{
			mysql_query("UPDATE chat SET ping_usuario='{$agora}' WHERE id='{$chat}'");
		}
		
	}
	
	public static function checarAusencia($chat)
	{
		$tempoAusencia=10; //segundos
		
		$r_chat=mysql_query("SELECT * FROM chat WHERE id='{$chat}'");
		$d_chat=mysql_fetch_array($r_chat);
		
		$t_agora=time();
		$t_usuario=strtotime($d_chat["ping_usuario"]);
		$t_operador=strtotime($d_chat["ping_operador"]);
		
		if($t_agora-$t_usuario > $tempoAusencia)
		{
			return("usuario");
		}

		if($t_agora-$t_operador > $tempoAusencia)
		{
			return("operador");
		}		
		
		return(null);
		
	}
	
	public static function atender($chat)
	{
		$operador=$_SESSION["operador"]["login"];
		$status="2";
		mysql_query("UPDATE chat SET status='{$status}', operador='{$operador}' WHERE id='{$chat}'");
		
		$a_dados=array();
		$a_dados["mensagem"]="* Operador iniciou atendimento!";
		$a_dados["usuario"]="";
		$a_dados["operador"]=$_SESSION["operador"]["login"];
		$a_dados["info"]="1";
		
		Chat::postar($chat,$a_dados);
	}
	
	public static function fechar($chat)
	{
		$status="1";
		mysql_query("UPDATE chat SET status='{$status}' WHERE id='{$chat}'");		
	}
	
	public static function comentar($chat,$comentario)
	{
		mysql_query("UPDATE chat SET comentario='{$comentario}' WHERE id='{$chat}'");		
	}
	
	public static function dados($chat)
	{
		$sqlcampos="";
		$sqlcampos.=", (SELECT nome FROM operadores WHERE operadores.login=chat.operador) AS nome_operador";
		
		$r_chat=mysql_query("SELECT *{$sqlcampos} FROM chat WHERE id='{$chat}'");
		$d_chat=mysql_fetch_array($r_chat);
		return($d_chat);		
	}
	
}
?>