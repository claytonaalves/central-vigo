<?
class Usuarios {
	
	public static function login($login,$senha,$empresa="")
	{
		
		global $vigo;
		
		$login=mysql_real_escape_string($login);
		$senha=mysql_real_escape_string($senha);
		$empresa=intval($empresa);
		
		$sqljoin="";
		$sqljoin.=" LEFT OUTER JOIN sistema_empresas ON (sistema_empresas.id=cadastro_clientes.idempresa)";
		
		$sqladd="";
		$sqladd.=($empresa!="")?" AND sistema_empresas.id='{$empresa}'":"";
		
		$sql="SELECT cadastro_clientes.* FROM cadastro_clientes{$sqljoin} WHERE cadastro_clientes.login='{$login}' AND cadastro_clientes.senha='{$senha}'{$sqladd}";
		
		$r_usuarios = mysql_query($sql,$vigo);
		
		if(mysql_num_rows($r_usuarios)==1)		
		{
		    $d_usuarios = mysql_fetch_array($r_usuarios);
		    $_SESSION["usuario"]=$d_usuarios;
		    return(true);
		}
		
		unset($_SESSION["usuario"]);
		return(false);
		
	}
	
	public static function logado()
	{
		if(isset($_SESSION["usuario"]))
		{
			return(true);
		}
		return(false);
	}
	
	public static function logoff()
	{
		unset($_SESSION["usuario"]);
		unset($_SESSION);
	}
	
	public static function contratoAssinado()
	{
		global $vigo;
		
		$r_central_contrato = mysql_query("SELECT * FROM cadastro_clientes WHERE id='{$_SESSION["usuario"]["id"]}' AND contrato_aceito='S'",$vigo);
		if(mysql_num_rows($r_central_contrato)>0)
		{
			return(true);
		}
		else
		{
			return(false);
		}
				
	}
	
	public static function foto($id="")
	{
		return("imagens/semfoto.jpg");
        
        global $vigo;
		
		$id=($id=="")?$_SESSION["usuario"]["id"]:intval($id);
		
		$r_usuarios = mysql_query("SELECT foto FROM cadastro_clientes WHERE id='{$id}' AND foto!=''",$vigo);
		
		if(mysql_num_rows($r_usuarios)>0)		
		{
		    $d_usuarios = mysql_fetch_array($r_usuarios);
		    
		    $arq_destino = "tmp/usuario_{$id}.jpg";
            
			$img_blob = imagecreatefromstring($d_usuarios['foto']);
		    if(imagejpeg($img_blob,$arq_destino,100))
		    {
		    	return($arq_destino);
		    }
		    else
		    {
		    	return("imagens/semfoto.jpg");
		    }
		    
 			//return("data:image/jpg;base64,".base64_encode($d_usuarios["foto"]));		    
		    
		}
		else
		{
			return("imagens/semfoto.jpg");
		}
		
	}
	
}
?>
