<?php
class Conexao {
	
	public static function conn($conn)
	{
        $inifile = parse_ini_file('config/vigo.conf', true);
		
        $a_conexao["vigo"] = array($inifile['vigo']['hostname'],
                                   $inifile['vigo']['database'],
                                   $inifile['vigo']['username'],
                                   $inifile['vigo']['password']);

        $a_conexao["mikrotik"] = array($inifile['mikrotik']['hostname'],
                                       $inifile['mikrotik']['database'],
                                       $inifile['mikrotik']['username'],
                                       $inifile['mikrotik']['password']);

	 $a_conexao["mail"] = array($inifile['mail']['hostname'],
                                    $inifile['mail']['database'],
                                    $inifile['mail']['username'],
                                    $inifile['mail']['password']); 
		
		$conexao = $a_conexao[$conn];
		
		$res=@mysql_connect($conexao[0],$conexao[2],$conexao[3]) OR exit("Erro na conex�o {$conn}!");
		@mysql_select_db($conexao[1],$res) OR exit("Erro na base de dados {$conn}!");
		
		return($res);
		
	} 
	
}
?>
